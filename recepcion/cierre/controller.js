

$("[data-tablemain]").dataTable();

App.cerrar = function (id, folio) {
    blackout.show('Cargando...', function () {
        $.ajax({
            type: "POST",
            url: "[:__BASE_URL:]index.acopio/cierre/get",
            data: "id=" + id
        }).done(function (data) {
            try {
                var r = JSON.parse(data);
                if (r.response === JSONTRUE) {
                    $("#ResumenReceipt").html(r.data.v);
                    $("#idrecepcion").val(id);
                    $("h5.form-title").html('Resumen Recepción ' + folio);
                    blackout.hide(function () {
                        $("[data-maincontainerform]").slideDown();
                    });
                } else {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha ocurrido un error recuperando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    });
                }
            } catch (e) {
                blackout.hide(function () {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>Ha ocurrido un error recuperando los datos</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                });
            }
        }).error(function () {
            blackout.hide(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>Ha ocurrido un error recuperando los datos</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
            });
        });
    });
};

App.aprobar = function (id) {
    blackout.show('Guardando...', function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;
        App.core.loadMysql(function () {
            var q = "SELECT * FROM fbr_recepcion WHERE idrecepcion = " + id + " LIMIT 1;";
            App.core.connection.query(q, function (err, query, fields) {
                if (!err) {
                    var qr = query[0];
                    App.core.loadMysql(function () {
                        var q = "SELECT * FROM fbr_recepcion_pesaje WHERE idrecepcion = " + qr.idrecepcion + ";";
                        console.log(q);
                        App.core.connection.query(q, function (err, row, fields) {
                            if (!err) {
                                var $all = row;
                                App.core.loadMysql(function () {
                                    var q = "INSERT INTO fbr_mov_envase (idtemporada,idacopio,idproductor,tipo_movimiento,flujo_ajuste,num_guia,patente,nombre_chofer,observacion,fecha_operacion,fecha_creacion,idcreador,activo)\n\
                                        VALUES(\n\
                                        " + App.GLOBAL.TEMPORADA + ",\n\
                                        " + App.GLOBAL.ACOPIO + ",\n\
                                        " + qr.idproductor + ",\n\
                                        1,\n\
                                        null,\n\
                                        0,\n\
                                        '',\n\
                                        '',\n\
                                        'Recepcion de Envase por cierre de Recepcion de Frutas',\n\
                                        '" + today + "',\n\
                                        '" + today + "',\n\
                                        " + App.GLOBAL.USUARIO + ",\n\
                                        1)";
                                    App.core.connection.query(q, function (err, row, fields) {
                                        if (!err) {
                                            var $l = row.insertId;
                                            App.core.loadMysql(function () {
                                                var q = "INSERT INTO fbr_mov_envase_detalle (idmov_envase,idenvase,cantidad)\n\
                                                                         VALUES ";
                                                for (var i = 0; i < $all.length; i++) {
                                                    var int = $all[i];
                                                    q += "(" + $l + "," + int.idenvase + "," + int.cant_bandejas + "),";

                                                }
                                                q = q.substring(0, q.length - 1);
                                                App.core.connection.query(q, function (err, query, fields) {
                                                    if (!err) {
                                                        App.core.loadMysql(function () {
                                                            var q = "UPDATE fbr_recepcion\n\
                                                                SET \n\
                                                                estado = 3,\n\
                                                                idaprobador = " + App.GLOBAL.USUARIO + "\n\
                                                                WHERE idrecepcion = " + id + ";";
                                                            App.core.connection.query(q, function (err, row, fields) {
                                                                if (!err) {
                                                                    blackout.hide(function () {
                                                                        $.gritter.add({
                                                                            title: "OK!",
                                                                            text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                                                            class_name: 'growl-success',
                                                                            sticky: false,
                                                                            time: 2000
                                                                        });
                                                                    });
                                                                } else {
                                                                    blackout.hide(function () {
                                                                        $.gritter.add({
                                                                            title: "Error",
                                                                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error al aprobar</i>",
                                                                            class_name: 'growl-danger',
                                                                            sticky: false,
                                                                            time: 4000
                                                                        });
                                                                        console.log(err);
                                                                    });
                                                                }
                                                            });
                                                        });
                                                    } else {
                                                        blackout.hide(function () {
                                                            $.gritter.add({
                                                                title: "Error",
                                                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error al aprobar</i>",
                                                                class_name: 'growl-danger',
                                                                sticky: false,
                                                                time: 4000
                                                            });
                                                            console.log(err);
                                                        });
                                                    }
                                                });
                                            });
                                        } else {
                                            blackout.hide(function () {
                                                $.gritter.add({
                                                    title: "Error",
                                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error al aprobar</i>",
                                                    class_name: 'growl-danger',
                                                    sticky: false,
                                                    time: 4000
                                                });
                                                console.log(err);
                                            });
                                        }
                                    });
                                });
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error al aprobar</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 4000
                                    });
                                    console.log(err);
                                });
                            }
                        });
                    });
                } else {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error al aprobar</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                        console.log(err);
                    });
                }
            });
        });
    });
};
App.comprobante = function (id) {
    blackout.show("Espere...", function () {
        var html = '<style>\n\
                *{\n\
                    font-family: Helvetica;\n\
                }\n\
                .maintable,\n\
                .maintable>tr,\n\
                .maintable>tr>td  {\n\
                    border: 0;\n\
                    border-collapse: collapse;\n\
                    font-size:10px;\n\
                }\n\
                table table, table table th, table table td {\n\
                    border: 1px solid black;\n\
                    border-collapse: collapse;\n\
                    font-size:10px;\n\
                }\n\
                table table.noborder, table table.noborder th, table table.noborder td {\n\
                    border: 0px solid black;\n\
                    border-collapse: collapse;\n\
                    font-size:10px;\n\
                }\n\
                .small{\n\
                    font-size:8px;\n\
                }\n\
                </style>';
        App.core.loadMysql(function () {
            var q = "SELECT * FROM fbr_v_recepciones WHERE idrecepcion = " + id + " LIMIT 1;";
            App.core.connection.query(q, function (err, row, fields) {
                if (!err) {
                    var head = row[0];
                    var $k_netos = head.kilos_netos_totales;
                    var $logo = '<img src="http://baladm.cl/framberry/wp-content/uploads/2016/05/logo_framberry.png" width="70" height="50">';
                    html += '<div>\n\
                    <table style="position:absolute;left:20px;top:20px; font-size:12px;" width="570" class="maintable">\n\
                        <tr>\n\
                            <td colspan=2>\n\
                                <table style="width:100%;">\n\
                                    <tr>\n\
                                        <th rowspan=2>' + $logo + '\
                                            <p style="font-size:8px;font-weight:normal;" class="small">Aseguramiento de calidad</p></th>\n\
                                        <th>PLANILLA COMPRA MATERIA PRIMA</th>\n\
                                        <th align="left"><p class="small">\n\
                                            Código : FR-SOPCC-CP-CL-01.01<br>\n\
                                            Fecha : Diciembre 2012<br>\n\
                                            Versión : 1 Rev. 04</p>\n\
                                        </th>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><p class="small">Elaborado por: Jefe Aseg. y C.C.</p></td>\n\
                                        <td><p class="small">Aprobado por: Gerente Adm. y Operaciones</p></td>\n\
                                    </tr>\n\
                                </table>\n\
                            </td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td colspan=2 style="padding: 6px 0px 8px 0px;">\n\
                                <table style="width:100%;" class="noborder">\n\
                                    <tr>\n\
                                        <td><p class=""><b>Fecha:</b> ' + head.fecha_operacion_i + '</p></td>\n\
                                        <td><p class=""><b>Rut:</b> ' + head.rut_productor + '</p></td>\n\
                                        <td><p class=""><b>Productor:</b> ' + head.nombre_productor + '</p></td>\n\
                                    </tr>\n\
                                </table>\n\
                            </td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td colspan=2 style="padding: 6px 0px 8px 0px;">\n\
                        ';
                    App.core.loadMysql(function () {
                        var q = "SELECT * FROM fbr_v_aux_variedad_cierre WHERE idrecepcion = " + id + ";";
                        App.core.connection.query(q, function (err, row, fields) {
                            if (!err) {
                                html += '<table style="width:100%;">\n\
                                        <tr align="center">\n\
                                            <th>Especie</th>\n\
                                            <th>Variedad</th>\n\
                                            <th>Tipo</th>\n\
                                        </tr>';
                                for (var i = 0; i < row.length; i++) {
                                    var rows = row[i];
                                    html += '<tr>';
                                    html += '<td>' + rows.especie + '</td>';
                                    html += '<td>' + rows.variedad + '</td>';
                                    html += '<td>' + rows.tipo + '</td>';
                                    html += '</tr>';
                                }
                                html += '</table>';
                                html += '</td>';
                                html += '</tr>';
                                html += '<tr>';
                                html += '<td colspan=2 style="padding: 6px 0px 8px 0px;">';
                                App.core.loadMysql(function () {
                                    var q = "SELECT * FROM fbr_v_aux_envase_cierre WHERE idrecepcion = " + id + ";";
                                    App.core.connection.query(q, function (err, row, fields) {
                                        if (!err) {
                                            html += '<table style="width:100%;">\n\
                                                    <tr align="center">\n\
                                                        <th>ENVASES</th>\n\
                                                        <th>CANTIDAD</th>\n\
                                                        <th>KILOS NETOS</th>\n\
                                                    </tr>';
                                            for (var i = 0; i < row.length; i++) {
                                                var rows = row[i];
                                                html += '<tr>';
                                                html += '<td>' + rows.envase + '</td>';
                                                html += '<td align="center">' + rows.all_bandejas + '</td>';
                                                html += '<td align="center">' + rows.all_kilos + '</td>';
                                                html += '</tr>';
                                            }
                                            html += '</table>';
                                            html += '</td>';
                                            html += '</tr>';
                                            html += '<tr>';
                                            App.core.loadMysql(function () {
                                                var q = "SELECT * FROM fbr_v_parametros_valores WHERE idrecepcion = " + id + " AND grupo_parametro = 1";
                                                App.core.connection.query(q, function (err, cal, fields) {
                                                    if (!err) {
                                                        App.core.loadMysql(function () {
                                                            var q = "SELECT * FROM fbr_v_parametros_valores WHERE idrecepcion = " + id + " AND grupo_parametro <> 1";
                                                            App.core.connection.query(q, function (err, cond, fields) {
                                                                if (!err) {
                                                                    //calidad
                                                                    html += '<td style="width:50%; padding: 6px 6px 8px 0px; vertical-align: super;">';
                                                                    html += '<table style="width:100%;">\n\
                                                                                <tr align="center">\n\
                                                                                    <td colspan="3"><b>CALIDAD</b></td>\n\
                                                                                </tr>\n\
                                                                                <tr align="center">\n\
                                                                                    <th>DEFECTOS</th>\n\
                                                                                    <th>PESO gr.</th>\n\
                                                                                    <th>%</th>\n\
                                                                                </tr>';
                                                                    var $pesoTotal = 0;
                                                                    var $percTotal = 0;
                                                                    var $allPulpa = 0;
                                                                    for (var i = 0; i < cal.length; i++) {
                                                                        var $cal = cal[i];
                                                                        var $percc = Math.round(($cal.valor * 100) / head.cant_muestra, 2);
                                                                        html += '<tr align="center">';
                                                                        html += '<td>' + $cal.parametro + '</td>';
                                                                        html += '<td>' + $cal.valor + '</td>';
                                                                        html += '<td>' + $percc + '%</td>';
                                                                        html += '</tr>';
                                                                        $pesoTotal += parseInt($cal.valor);
                                                                        $percTotal += parseInt($percc);
                                                                    }
                                                                    $allPulpa += $percTotal;
                                                                    html += '<tr align="center">\n\
                                                                            <td><b>TOTALES</b></td>\n\
                                                                            <td>' + $pesoTotal + '</td>\n\
                                                                            <td>' + parseInt($percTotal) + '%</td>\n\
                                                                        </tr>';
                                                                    html += '</table>';
                                                                    html += '</td>';
                                                                    //condicion
                                                                    html += '<td style="width:50%; padding: 6px 0px 8px 6px; vertical-align: super;">';
                                                                    html += '<table style="width:100%;">\n\
                                                                                <tr align="center">\n\
                                                                                    <td colspan="3"><b>CONDICI&Oacute;N</b></td>\n\
                                                                                </tr>\n\
                                                                                <tr align="center">\n\
                                                                                    <th>DEFECTOS</th>\n\
                                                                                    <th>PESO gr.</th>\n\
                                                                                    <th>%</th>\n\
                                                                                </tr>';
                                                                    var $pesoTotalCondicion = 0;
                                                                    var $percTotalCondicion = 0;
                                                                    for (var i = 0; i < cond.length; i++) {
                                                                        var $cond = cond[i];
                                                                        var $perc = Math.round(($cond.valor * 100) / head.cant_muestra, 2);
                                                                        html += '<tr align="center">';
                                                                        html += '<td>' + $cond.parametro + '</td>';
                                                                        html += '<td>' + $cond.valor + '</td>';
                                                                        html += '<td>' + $perc + '%</td>';
                                                                        html += '</tr>';
                                                                        $pesoTotalCondicion += parseInt($cond.valor);
                                                                        $percTotalCondicion += parseInt($perc);
                                                                    }
                                                                    $allPulpa += $percTotalCondicion;
                                                                    html += '<tr align="center">\n\
                                                                            <td><b>TOTALES</b></td>\n\
                                                                            <td>' + $pesoTotalCondicion + '</td>\n\
                                                                            <td>' + parseInt($percTotalCondicion) + '%</td>\n\
                                                                        </tr>';
                                                                    html += '</table>';
                                                                    html += '</td>';
                                                                    html += '</tr>';
                                                                    //analisis

                                                                    html += '<tr>';
                                                                    html += '<td style="width:50%; padding: 6px 0px 8px 0px;">';
                                                                    html += '<table style="width:100%;">\n\
                                                                                <tr align="center">\n\
                                                                                    <td colspan="3"><b>RESULTADO AN&Aacute;LISIS</b></td>\n\
                                                                                </tr>\n\
                                                                                <tr align="center">\n\
                                                                                    <td><b>CALIDAD</b></td>\n\
                                                                                    <td><b>KILOS</b></td>\n\
                                                                                    <td><b>%</b></td>\n\
                                                                                </tr><tr align="center">\n\
                                                                                    <td>IQF</td>\n\
                                                                                    <td>' + App.round((((100 - $allPulpa) * $k_netos) / 100), 2) + '</td>\n\
                                                                                    <td>' + (100 - $allPulpa) + '%</td>\n\
                                                                                </tr><tr align="center">\n\
                                                                                    <td>PULPA</td>\n\
                                                                                    <td>' + App.round((($allPulpa * $k_netos) / 100), 2) + '</td>\n\
                                                                                    <td>' + $allPulpa + '%</td>\n\
                                                                                </tr></table>';
                                                                    html += '</td>';
                                                                    html += '<td style="width:50%; padding: 6px 0px 8px 0px;"></td>';
                                                                    html += '</tr>';
                                                                    html += '<tr>';
                                                                    html += '<td colspan=2 style="padding: 6px 0px 8px 0px;">';
                                                                    html += '<table style="width:100%;">\n\
                                                                                <tr>\n\
                                                                                    <td style="border:0;"><b>OBSERVACIONES</b></td>\n\
                                                                                </tr>\n\
                                                                                <tr>\n\
                                                                                    <td style="height: 12px !important;"><font size="1">' + head.observaciones + '</td>\n\
                                                                                </tr>\n\
                                                                        </table>';
                                                                    html += '</td>';
                                                                    html += '</tr>';
                                                                    html += '<tr>';
                                                                    html += '<td colspan=2 style="padding: 6px 0px 8px 0px;">';
                                                                    html += '<table align="left" style="margin-top:15px; font-size:12px;border:0;margin-top:80px;" width="560">\n\
                                                                                <tr align="center">\n\
                                                                                    <td style="width:7%;border:0;"></td>\n\
                                                                                    <td style="width:35%;border:0;border-top:1px solid black !important;"><b>PRODUCTOR</b></td>\n\
                                                                                    <td style="width:16%;border:0;"></td>\n\
                                                                                    <td style="width:35%;border:0;border-top:1px solid black !important;"><b>CONTROL DE CALIDAD</b></td>\n\
                                                                                    <td style="width:7%;border:0;"></td>\n\
                                                                                </tr>\n\
                                                                        </table>';
                                                                    html += '</td>';
                                                                    html += '</tr>';
                                                                    html += '</table>';
                                                                    App.core.pdf.create(html, {"format": "Letter"}).toStream(function (err, stream) {
                                                                        if (!err) {
                                                                            const {dialog} = require('electron').remote;
                                                                            blackout.hide(function () {
                                                                                dialog.showSaveDialog({
                                                                                    title: "Guardar Reporte",
                                                                                    defaultPath: "Comprobante.pdf"
                                                                                },
                                                                                        function (fileName) {
                                                                                            stream.pipe(App.core.fs.createWriteStream(fileName));
                                                                                        });
                                                                            });
                                                                        } else {
                                                                            console.log(err);
                                                                            blackout.hide();
                                                                        }
                                                                    });
                                                                } else {
                                                                    console.log(err);
                                                                    blackout.hide();
                                                                }
                                                            });
                                                        });
                                                    } else {
                                                        console.log(err);
                                                        blackout.hide();
                                                    }
                                                });
                                            });
                                        } else {
                                            console.log(err);
                                            blackout.hide();
                                        }
                                    });
                                });
                            }
                        });
                    });
                } else {
                    console.log(err);
                    blackout.hide();
                }
            });
        });
    });
};
//$("#primal-form").bind('submit', function (e) {
//    e.preventDefault();
//    e.stopPropagation();
//    xmodal.show(null, "¿Está seguro que desea aprobar esta recepción?", function () {
//        $.ajax({
//            type: "POST",
//            url: "[:__BASE_URL:]index.acopio/cierre/close",
//            data: "id=" + $("#idrecepcion").val()
//        }).done(function (data) {
//            try {
//                var r = JSON.parse(data);
//                if (r.response === JSONTRUE) {
//                    App.oTable.fnDraw();
//                    blackout.hide(function () {
//                        $("[data-maincontainerform]").slideUp();
//                    });
//                    $.gritter.add({
//                        title: "OK!",
//                        text: "<i class='fa fa-clock-o'></i> <i>Se ha cerrados correctamente la recepción</i>",
//                        class_name: 'growl-success',
//                        sticky: false,
//                        time: 2000
//                    });
//                } else {
//                    var extime = (typeof r.message !== "undefined") ? 6000 : 2000;
//                    var extmsg = (typeof r.message !== "undefined") ? ':<br /><br /><b>' + r.message + '</b>' : '';
//                    $.gritter.add({
//                        title: "Error",
//                        text: "<i class='fa fa-clock-o'></i> <i>Ha ocurrido un error mientras se cerraba la recepción" + extmsg + "</i>",
//                        class_name: 'growl-danger',
//                        sticky: false,
//                        time: extime
//                    });
//                }
//                xmodal.hide();
//            } catch (e) {
//                $.gritter.add({
//                    title: "Error",
//                    text: "<i class='fa fa-clock-o'></i> <i>Ha ocurrido un error mientras se cerraba la recepción</i>",
//                    class_name: 'growl-danger',
//                    sticky: false,
//                    time: 2000
//                });
//                xmodal.hide();
//            }
//        }).error(function () {
//            $.gritter.add({
//                title: "Error",
//                text: "<i class='fa fa-clock-o'></i> <i>" + App.xDom.deleteMessageFail + "</i>",
//                class_name: 'growl-danger',
//                sticky: false,
//                time: 2000
//            });
//            xmodal.hide();
//        });
//    });
//});