/* global blackout, App, xmodal */
var xmodal = function () {
    var loadInnerModal = function (callback) {
        callback || (callback = function () {
        });
        var bot = document.getElementById('xmodalHandler');
        bot.innerHTML = '<div class="heading"></div>\n\
                            <div class="content">\n\
                                <p class="bodymodal">\n\
                                </p>\n\
                                <p class="buttons">\n\
                                    <button type="button" class="btn btn-w-m btn-primary acceptdeal">Aceptar</button>\n\
                                    <button type="button" class="btn btn-w-m btn-danger canceldeal">Cancelar</button>\n\
                                </p>\n\
                            </div>';
        callback();
    };

    var init = function (callback) {
        var body = document.getElementsByTagName('body')[0];
        if (!document.getElementById('reveal-xmodal-bg')) {
            var xm = document.createElement('div');
            xm.id = 'reveal-xmodal-bg';
            xm.addEventListener('click', function () {
                xmodal.hide();
            });
            body.appendChild(xm);
//            xm.onload = function() {
            var bo = document.createElement('div');
            bo.id = 'xmodalHandler';
            body.appendChild(bo);
            bo.onload = loadInnerModal(callback);
//            };
        } else if (!document.getElementById('xmodalHandler')) {
            var bo = document.createElement('div');
            bo.id = 'xmodalHandler';
            body.appendChild(bo);
            bo.onload = loadInnerModal(callback);
        } else {
            loadInnerModal(callback);
        }
        return true;
    };

    var hideModal = function (callback) {
        $("#xmodalHandler, #reveal-xmodal-bg").fadeOut(function () {
            callback();
        });
    };
    return {
        show: function (title, body, acceptdeal, canceldeal, onopen) {
            init(function () {
                canceldeal || (canceldeal = function () {
                    xmodal.hide();
                });
                onopen || (onopen = function () {});
                if (title) {
                    $("#xmodalHandler .heading").html(String(title));
                } else {
                    $("#xmodalHandler .heading").remove();
                    $("#xmodalHandler .content").addClass('fullradius');
                }
                $("#xmodalHandler .content p.bodymodal").html(String(body));
                $("#xmodalHandler .acceptdeal").bind('click', acceptdeal);
                $("#xmodalHandler .canceldeal").bind('click', canceldeal);
                $("#xmodalHandler, #reveal-xmodal-bg").fadeIn(function () {
                    onopen();
                });
            });
        },
        hide: function (callback) {
            callback || (callback = function () {});
            hideModal(callback);
        }
    };
}();

$("[data-tablemain]").dataTable();

$("[data-addnew]").bind('click', function (e) {
    e.stopPropagation();
    e.preventDefault();
    var form = '#primal-form';
    $('[data-maincontainerform]').slideUp();
    $(form + ' select').val(null).trigger('chosen:updated');
    $('[data-containercalcond]').slideUp();
    $(".calidad_params.pparams tbody").html('');
    $(".condicion_params.pparams tbody").html('');
    $('[data-maincontainerform]').slideDown;
});

$("#primal-form").validate({
    rules: {
        idproductor: {
            required: true
        },
        idproducto: {
            required: true
        },
        tipo_moneda: {
            required: true
        },
        fecha_operacion: {
            required: true
        },
        num_guia: {
            required: true
        },
        patente: {
            required: true
        },
        chofer: {
            required: true
        },
        cant_muestra: {
            required: true,
            min: 0
        }
    },
    messages: {
        idproductor: {
            required: "Debe seleccionar un Productor"
        },
        idproducto: {
            required: "Debe seleccionar un Producto"
        },
        tipo_moneda: {
            required: "Debe seleccionar una Moneda"
        },
        fecha_operacion: {
            required: "Debe ingresar una fecha de operacion"
        },
        num_guia: {
            required: "Debe ingresar un numero de guia de despacho"
        },
        patente: {
            required: "Debe ingresar una patente"
        },
        chofer: {
            required: "Debe ingresar un chofer"
        },
        cant_muestra: {
            required: "Debe ingresar una cantidad de muestra",
            min: "Debe ingresar un número válido"
        }
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (form) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        today = yyyy + '-' + mm + '-' + dd;
        var qaparams = JSON.parse(App.UICalidad.serializeTable());
//        var dataForm = $(form).serialize();
        var idpk = parseInt($(".pk_form").val());
        $('table.pparams').find('input').change();
        if (App.UICalidad.verifyErrorParams()) {
            if (idpk === 0) {
                blackout.show("Guardando...", function () {
                    var fop = $("[name=fecha_operacion]").val();
                    var idproducto = $("[name=idproducto]").val();
                    var idproductor = $("[name=idproductor]").val();
                    var num_guia = $("[name=num_guia]").val();
                    var patente = $("[name=patente]").val();
                    var chofer = $("[name=chofer]").val();
                    var cant_muestra = $("[name=cant_muestra]").val();
                    var $moneda = $("[name=tipo_moneda]").val();
                    var cant = $('[name=cant_muestra]').val();
                    try {
                        App.core.loadMysql(function () {
                            var q = "SELECT * FROM fbr_precio_semanal \n\
                            WHERE idacopio = " + App.GLOBAL.ACOPIO + "\n\
                            AND semana_numero = CONCAT(if(WEEK('" + fop + "', 3)<10,CONCAT('0',WEEK('" + fop + "', 3)),WEEK('" + fop + "', 3)), '-', DATE_FORMAT('" + fop + "', '%Y'))\n\
                            AND idproducto = " + idproducto + "\n\
                            AND idproductor in (" + idproductor + ",0)\n\
                            ORDER BY idproductor DESC LIMIT 1";
                            console.log(q);
                            App.core.connection.query(q, function (err, row, fields) {
                                if (!err) {
                                    var $mon = null;
                                    console.log(row.length);
                                    if (row.length > 0) {
                                        var $ps = row[0];
                                        if (parseInt($moneda) === 1) {
                                            if (parseInt($ps.precio_iqf) !== 0) {
                                                $mon = $ps;
                                            } else {
                                                blackout.hide(function () {
                                                    console.log('precio_iqf == null');
                                                });
                                            }
                                        } else {
                                            if (parseInt($ps.precio_iqf_usd) !== 0) {
                                                $mon = $ps;
                                            } else {
                                                blackout.hide(function () {
                                                    console.log('precio_iqf_usd == null');
                                                });
                                            }
                                        }
                                        if ($mon === null) {
                                            blackout.hide(function () {
                                                $.gritter.add({
                                                    title: "Alerta!",
                                                    text: "<i class='fa fa-clock-o'></i> <i>No Existen Valores Semanales para este producto, en esta fecha de operación: " + fop + ". Ingreselos para poder guardar</i>",
                                                    class_name: 'growl-alert',
                                                    sticky: false,
                                                    time: 2000
                                                });
                                            });
                                        } else {
                                            var $t = 0;
                                            var $res_iqf = 0;
                                            for (var i = 0; i < qaparams.length; i++) {
                                                $t += parseInt(qaparams[i].value);
                                            }
                                            if (cant !== 0) {
                                                $res_iqf = ($t * 100) / parseInt(cant);
                                            } else {
                                                $res_iqf = 0;
                                            }
                                            App.core.loadMysql(function () {
                                                var q = "SELECT * FROM fbr_recepcion ORDER BY correlativo DESC LIMIT 1";
                                                console.log(q);
                                                App.core.connection.query(q, function (err, row, fields) {
                                                    if (!err) {
                                                        var correlativo = 1;
                                                        if (row.length > 0) {
                                                            var $a = row[0];
                                                            if ($a !== null) {
                                                                correlativo = (($a.correlativo) + 1);
                                                            }
                                                        }
                                                        App.core.loadMysql(function () {
                                                            var q = "\
                                                            INSERT INTO \n\
                                                            fbr_recepcion(\n\
                                                                idacopio,\n\
                                                                idtemporada,\n\
                                                                idproductor,\n\
                                                                idproducto,\n\
                                                                fecha_operacion,\n\
                                                                correlativo,\n\
                                                                num_guia,\n\
                                                                patente,\n\
                                                                chofer,\n\
                                                                cant_muestra,\n\
                                                                res_iqf,\n\
                                                                res_pulpa,\n\
                                                                tipo_moneda,\n\
                                                                precio_iqf,\n\
                                                                precio_pulpa,\n\
                                                                precio_iqf_usd,\n\
                                                                precio_pulpa_usd,\n\
                                                                estado,\n\
                                                                observaciones,\n\
                                                                estado_envio,\n\
                                                                fecha_creacion,\n\
                                                                idcreador,\n\
                                                                activo)\n\
                                                        VALUES(\n\
                                                         " + App.GLOBAL.ACOPIO + ",\n\
                                                         " + App.GLOBAL.TEMPORADA + ",\n\
                                                         " + idproductor + ",\n\
                                                         " + idproducto + ",\n\
                                                         '" + fop + "', \n\
                                                         " + correlativo + ",\n\
                                                         '" + num_guia + "',\n\
                                                         '" + patente + "',\n\
                                                         '" + chofer + "',\n\
                                                         " + cant_muestra + ",\n\
                                                         " + parseInt($res_iqf) + ",\n\
                                                         " + (100 - parseInt($res_iqf)) + ",\n\
                                                         " + $moneda + ",\n\
                                                         " + $mon.precio_iqf + ",\n\
                                                         " + $mon.precio_iqf + ",\n\
                                                         " + parseInt($mon.precio_iqf_usd) + ",\n\
                                                         " + parseInt($mon.precio_iqf_usd) + ",\n\
                                                         1,\n\
                                                         '" + $('[name=observaciones]').val() + "',\n\
                                                         0,\n\
                                                         '" + today + "',\n\
                                                         " + App.GLOBAL.USUARIO + ",\n\
                                                         1);";
                                                            App.core.connection.query(q, function (err, row, fields) {
                                                                if (!err) {
                                                                    var $l = row.insertId;
                                                                    App.core.loadMysql(function () {
                                                                        var q = "INSERT INTO fbr_recepcion_parametro_valor (idparametro,idrecepcion,valor)\n\
                                                                         VALUES ";
                                                                        for (var i = 0; i < qaparams.length; i++) {
                                                                            var int = qaparams[i];
                                                                            q += "(" + int.id + "," + $l + "," + int.value + "),";

                                                                        }
                                                                        q = q.substring(0, q.length - 1);
                                                                        App.core.connection.query(q, function (err, row, fields) {
                                                                            blackout.hide(function () {
                                                                                App.reloadTable();
                                                                                $.gritter.add({
                                                                                    title: "OK!",
                                                                                    text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                                                                    class_name: 'growl-success',
                                                                                    sticky: false,
                                                                                    time: 2000
                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                } else {
                                                                    blackout.hide(function () {
                                                                        console.log(err);
                                                                        console.log("Loss :" + q);
                                                                    });
                                                                }
                                                            });
                                                        });
                                                    } else {
                                                        blackout.hide(function () {
                                                            $.gritter.add({
                                                                title: "Error",
                                                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error cargando el correlativo</i>",
                                                                class_name: 'growl-danger',
                                                                sticky: false,
                                                                time: 4000
                                                            });
                                                        });
                                                        console.log(err);
                                                    }
                                                });
                                            });
                                        }
                                    } else {
                                        blackout.hide(function () {
                                            $.gritter.add({
                                                title: "Alerta!",
                                                text: "<i class='fa fa-clock-o'></i> <i>No Existen Valores Semanales para este producto, en esta fecha de operación: " + fop + ". Ingreselos para poder guardar</i>",
                                                class_name: 'growl-alert',
                                                sticky: false,
                                                time: 2000
                                            });
                                        });
                                    }
                                } else {
                                    blackout.hide(function () {
                                        console.log(q);
                                        console.log(err);
                                    });
                                }
                            });
                        });
                    } catch (e) {
                        console.log(e);
                    }

                });
            } else {
                var qaparams = JSON.parse(App.UICalidad.serializeTable());
                blackout.show("Editando...", function () {
                    var fop = $("[name=fecha_operacion]").val();
                    var idproducto = $("[name=idproducto]").val();
                    var idproductor = $("[name=idproductor]").val();
                    var num_guia = $("[name=num_guia]").val();
                    var patente = $("[name=patente]").val();
                    var chofer = $("[name=chofer]").val();
                    var cant_muestra = $("[name=cant_muestra]").val();
                    var $moneda = $("[name=tipo_moneda]").val();
                    var cant = $('[name=cant_muestra]').val();
                    App.core.loadMysql(function () {
                        var q = "SELECT * FROM fbr_precio_semanal \n\
                            WHERE idacopio = " + App.GLOBAL.ACOPIO + "\n\
                            AND '" + fop + "' between fecha_desde and fecha_hasta\n\
                            AND idproducto = " + idproducto + "\n\
                            AND idproductor in (" + idproductor + ",0)\n\
                            ORDER BY idproductor DESC LIMIT 1";
                        App.core.connection.query(q, function (err, row, fields) {
                            if (!err) {
                                var $ps = row[0];
                                var $mon = null;
                                if (parseInt($moneda) === 1) {
                                    if (parseInt($ps.precio_iqf) !== 0) {
                                        $mon = $ps;
                                    }
                                } else {
                                    if (parseInt($ps.precio_iqf_usd) !== 0) {
                                        $mon = $ps;
                                    } else {
                                        blackout.hide(function () {
                                            $.gritter.add({
                                                title: "Error!",
                                                text: "<i class='fa fa-clock-o'></i> <i>Error al editar los datos</i>",
                                                class_name: 'growl-danger',
                                                sticky: false,
                                                time: 2000
                                            });
                                        });
                                    }
                                }
                                if ($mon === null) {
                                    blackout.hide(function () {
                                        $.gritter.add({
                                            title: "Alerta!",
                                            text: "<i class='fa fa-clock-o'></i> <i>No Existen Valores Semanales para este producto, en esta fecha de operación: " + fop + ". Ingreselos para poder guardar</i>",
                                            class_name: 'growl-alert',
                                            sticky: false,
                                            time: 2000
                                        });
                                    });
                                }
                                var $t = 0;
                                var $res_iqf = 0;
                                for (var i = 0; i < qaparams.length; i++) {
                                    $t += parseInt(qaparams[i].value);
                                }
                                if (cant !== 0) {
                                    $res_iqf = ($t * 100) / parseInt(cant);
                                } else {
                                    $res_iqf = 0;
                                }
                                App.core.loadMysql(function () {
                                    var q = "UPDATE fbr_recepcion\n\
                                                SET\n\
                                                 idproductor = " + idproductor + ",\n\
                                                 idproducto = " + idproducto + ",\n\
                                                 fecha_operacion = '" + fop + "', \n\
                                                 num_guia = '" + num_guia + "',\n\
                                                 patente = '" + patente + "',\n\
                                                 chofer = '" + chofer + "',\n\
                                                 cant_muestra = " + cant_muestra + ",\n\
                                                 res_iqf = " + parseInt($res_iqf) + ",\n\
                                                 res_pulpa = " + (100 - parseInt($res_iqf)) + ",\n\
                                                 tipo_moneda = " + $moneda + ",\n\
                                                 precio_iqf = " + $mon.precio_iqf + ",\n\
                                                 precio_pulpa = " + $mon.precio_iqf + ",\n\
                                                 precio_iqf_usd = " + parseInt($mon.precio_iqf_usd) + ",\n\
                                                 precio_pulpa_usd = " + parseInt($mon.precio_iqf_usd) + ",\n\
                                                 observaciones = '" + $('[name=observaciones]').val() + "',\n\
                                                 idmodificador = " + App.GLOBAL.USUARIO + ",\n\
                                                 fecha_edicion = '" + today + "' WHERE idrecepcion = " + idpk + " ;";
                                    App.core.connection.query(q, function (err, row, fields) {
                                        if (!err) {
                                            var $l = $(".pk_form").val();
                                            App.core.loadMysql(function () {
                                                var q = "DELETE FROM fbr_recepcion_parametro_valor WHERE idrecepcion = " + $l + ";";
                                                App.core.connection.query(q, function (err, row, fields) {
                                                    if (!err) {
                                                        App.core.loadMysql(function () {
                                                            var q = "INSERT INTO fbr_recepcion_parametro_valor (idparametro,idrecepcion,valor)\n\
                                                                         VALUES ";
                                                            for (var i = 0; i < qaparams.length; i++) {
                                                                var int = qaparams[i];
                                                                q += "(" + int.id + "," + $l + "," + int.value + "),";

                                                            }
                                                            q = q.substring(0, q.length - 1);
                                                            App.core.connection.query(q, function (err, row, fields) {
                                                                if (!err) {
                                                                    blackout.hide(function () {
                                                                        App.reloadTable();
                                                                        $.gritter.add({
                                                                            title: "OK!",
                                                                            text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                                                            class_name: 'growl-success',
                                                                            sticky: false,
                                                                            time: 2000
                                                                        });
                                                                    });
                                                                    $('select').val('').trigger('chosen:updated');
                                                                    $('input').val('');
                                                                } else {
                                                                    blackout.hide(function () {
                                                                        console.log(err);
                                                                    });
                                                                }
                                                            });
                                                        });
                                                    } else {
                                                        blackout.hide(function () {
                                                            console.log(err);
                                                        });
                                                    }
                                                });
                                            });
                                        } else {
                                            blackout.hide(function () {
                                                console.log(err);
                                            });
                                        }
                                    });
                                });

                            } else {
                                blackout.hide(function () {
                                    console.log(err);
                                });
                            }
                        });
                    });
                });
            }
        } else {
            App.UICalidad.focusQAError();
        }
    }
});

App.editar = function (idpk) {
    blackout.show('Cargando....', function () {
        App.core.loadMysql(function () {
            var q = "SELECT \n\
                    `fbr_recepcion`.`idrecepcion`,\n\
                    `fbr_recepcion`.`tipo_moneda`,\n\
                    `fbr_recepcion`.`idproductor`,\n\
                    `fbr_recepcion`.`idproducto`,\n\
                    date_format(`fbr_recepcion`.`fecha_operacion`,'%Y-%m-%d') as fecha_operacion,\n\
                    `fbr_recepcion`.`num_guia`,\n\
                    `fbr_recepcion`.`patente`,\n\
                    `fbr_recepcion`.`chofer`,\n\
                    `fbr_recepcion`.`cant_muestra`,\n\
                    `fbr_recepcion`.`observaciones`\n\
                 FROM fbr_recepcion WHERE idrecepcion = " + idpk + " LIMIT 1;";
            console.log(q);
            App.core.connection.query(q, function (err, row, fields) {
                if (!err) {
                    $('[data-maincontainerform]').slideUp();
                    blackout.hide(function () {
                        var $qr = row[0];
                        $(".pk_form").val($qr.idrecepcion);
                        $("[name=tipo_moneda]").val($qr.tipo_moneda).trigger('chosen:updated');
                        $("[name=idproductor]").val($qr.idproductor).trigger('chosen:updated');
                        $("[name=idproducto]").val($qr.idproducto).trigger('chosen:updated');
                        $("[name=idproducto]").change();
                        $("[name=fecha_operacion]").val($qr.fecha_operacion);
//                        console.log($qr.fecha_operacion);
                        $("[name=num_guia]").val($qr.num_guia);
                        $("[name=patente]").val($qr.patente);
                        $("[name=chofer]").val($qr.chofer);
                        $("[name=cant_muestra]").val($qr.cant_muestra);
                        $("[name=observaciones]").val($qr.observaciones);
                        $('[data-maincontainerform]').slideDown();
                    });
                } else {
                    console.log(err);
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error cargando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                }
            });
        });
    });
};

App.eliminar = function (idpk) {
    xmodal.show(null, 'Esta seguro de que desea eliminar este registro?', function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;
        blackout.hide(function () {
            App.core.loadMysql(function () {
                var q = "UPDATE fbr_recepcion\n\
                SET activo = 0,\n\
                    ideliminador = " + App.GLOBAL.USUARIO + ",\n\
                    fecha_eliminacion = '" + today + "'\
                WHERE idrecepcion = " + idpk + ";";
                console.log(q);
                App.core.connection.query(q, function (err, row, fields) {
                    if (!err) {
                        blackout.hide(function () {
                            App.reloadTable();
                            $.gritter.add({
                                title: "OK!",
                                text: "<i class='fa fa-clock-o'></i> <i>Se han eliminado correctamente los datos</i>",
                                class_name: 'growl-success',
                                sticky: false,
                                time: 2000
                            });
                            callback();
                        });
                    } else {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error eliminando los datos</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 4000
                            });
                            callback();
                        });
                    }
                });
            });
        });
    });
};

App.ui = function () {
};

App.ui.prototype.init = function () {
    $('[name="patente"]').mask('AA-AA-99', {
        translation: {
            placeholder: "  -  -  "
        }
    });
    $('[name="cant_muestra"], .pparams input').bind('change blur focus keyup', function () {
        App.UICalidad.verifyValue();
        App.UICalidad.calculateIQF();
    });
    this.changeProductos();
};

App.ui.prototype.verifyValue = function (obj) {
    if (typeof obj !== "undefined") {
        var max = parseFloat($('[name="cant_muestra"]').val());
        var val = parseFloat($(obj).val());
        var message = $(document.createElement('label')).addClass('error errorlbl');
        var trg = $(obj).closest('td');
        if (max >= 0) {
            if (val >= 0) {
                if (trg.find('.errorlbl').length !== 0) {
                    trg.find('.errorlbl').remove();
                }
                if (val > max) {
                    $(obj).closest('table').attr('data-error-calidad', true);
                    message.html('Debe Ingresar un valor menor o igual a ' + max);
                    if (trg.find('.errorlbl').length === 0) {
                        trg.append(message);
                    }
                } else {
                    this.verifyErrorParams();
                }
            } else {
                $(obj).closest('table').attr('data-error-calidad', 1);
                message.html('Debe Ingresar un valor mayor o igual a 0');
                if (trg.find('.errorlbl').length === 0) {
                    trg.append(message);
                }
            }
        }
    } else {
        $('table.pparams input').change();
    }
};

App.ui.prototype.verifyErrorParams = function () {
    if ($('table.pparams').find('.errorlbl').length === 0) {
        $('table.pparams').attr('data-error-calidad', 0);
        return true;
    } else {
        $('table.pparams').attr('data-error-calidad', 1);
        return false;
    }
};

App.ui.prototype.focusQAError = function () {
    $($('table.pparams').find('.errorlbl')[0]).closest('tr').find('input').focus();
};

App.ui.prototype.calculateIQF = function () {
    var t = 0;
    var tot_danos = $('[data-name="tot_danos"]');
    var porcentaje_export = $('[data-name="porcentaje_export"]');
    var all = parseFloat($('[name="cant_muestra"]').val());
    $('.pparams').each(function () {
        $(this).find('tbody tr').each(function () {
            var $this = $(this);
            var i = $this.find('td:last-child input');
            t += parseFloat(i.val());
        });
    });
    if (all === 0 || isNaN(all)) {
        tot_danos.val(0);
        porcentaje_export.val(0);
    } else {
        var p = ((Math.round(((t * 100) / all) * 100)) / 100);
        tot_danos.val(p);
        porcentaje_export.val((100 - p));
    }
};

App.ui.prototype.changeProductos = function () {
    $('[name="idproducto"]').bind('change', function (e) {
        var idpk = $(".pk_form").val();
        if ($(this).val() + "" !== "") {
            if (!$("[data-containercalcond]").is(':visible')) {
                $("[data-containercalcond]").slideDown();
            }
        }
        var $id = parseInt($("#idrecepcion").val());
        var v = $(this).val();
        blackout.show('Cargando...', function () {
            var r = "";
            var q = "SELECT * FROM fbr_producto WHERE idproducto = '" + v + "'";
            App.core.loadMysql(function () {
                App.core.connection.query(q, function (err, rows, fields) {
                    if (!err) {
                        var $ep = rows[0];
                        var $v = $ep.idvariedad;
                        var q = "SELECT * FROM fbr_variedad WHERE idvariedad = '" + $v + "'";
                        App.core.loadMysql(function () {
                            App.core.connection.query(q, function (err, rows, fields) {
                                if (!err) {
                                    var $esp = rows[0].idespecie;
                                    var q = "SELECT * FROM fbr_param_especie WHERE idespecie = '" + $esp + "'";
                                    App.core.loadMysql(function () {
                                        App.core.connection.query(q, function (err, rows, fields) {
                                            if (!err) {
                                                var $in = "0";
                                                for (var i = 0; i < rows.length; i++) {
                                                    $in += ',' + rows[i].idparametro;
                                                }
                                                var q = "SELECT * FROM fbr_parametro WHERE idparametro in (" + $in + ") AND activo=1";
                                                App.core.loadMysql(function () {
                                                    App.core.connection.query(q, function (err, $par, fields) {
                                                        if (!err) {
                                                            var q = "SELECT * FROM fbr_recepcion_parametro_valor WHERE idrecepcion  = '" + $id + "'";
                                                            App.core.loadMysql(function () {
                                                                App.core.connection.query(q, function (err, $vals, fields) {
                                                                    if (!err) {
                                                                        var $rvals = {};
                                                                        for (var $i = 0; $i < $vals.length; $i++) {
                                                                            $rvals[$vals[$i].idparametro] = $vals[$i].valor;
                                                                        }
                                                                        var $rca = '';
                                                                        var $rco = '';
                                                                        for (var $i = 0; $i < $par.length; $i++) {
                                                                            var $pr = $par[$i];
                                                                            var $value = "";
                                                                            if (typeof $rvals[$pr.idparametro] !== "undefined") {
                                                                                $value = ' value="' + $rvals[$pr.idparametro] + '" ';
                                                                            } else {
                                                                                $value = ' value="0" ';
                                                                            }
                                                                            if ($pr.grupo_parametro == 1) {
                                                                                $rca += '<tr>';
                                                                                $rca += '<td>' + $pr.parametro + '</td>';
                                                                                $rca += '<td><input type="number" class="form-control" data-name="idparam_' + $pr.idparametro + '" placeholder="' + $pr.parametro + '" ' + $value + ' required="required"></td>';
                                                                                $rca += '</tr>';
                                                                            } else {
                                                                                $rco += '<tr>';
                                                                                $rco += '<td>' + $pr.parametro + '</td>';
                                                                                $rco += '<td><input type="number" class="form-control" data-name="idparam_' + $pr.idparametro + '" placeholder="' + $pr.parametro + '" ' + $value + ' required="required"></td>';
                                                                                $rco += '</tr>';
                                                                            }

                                                                        }
                                                                        $(".calidad_params.pparams tbody").html($rca);
                                                                        $(".condicion_params.pparams tbody").html($rco);
                                                                        setTimeout(function () {
                                                                            App.UICalidad.calculateIQF();
                                                                            $('.pparams input').bind('change blur focus keyup', function () {
                                                                                App.UICalidad.calculateIQF();
                                                                            });
                                                                        }, 10);
                                                                        blackout.hide(function () {
                                                                            $('.pparams tbody tr td:last-child input, [name="cant_muestra"]').each(function () {
                                                                                $(this).bind('keyup change blur focus', function (e) {
                                                                                    if (App.UICalidad.verifyValue(this)) {
                                                                                        App.UICalidad.calculateIQF();
                                                                                    }
                                                                                });
                                                                            });
                                                                        });

                                                                    } else {
                                                                        console.log(err);
                                                                    }
                                                                });
                                                            });

                                                        } else {
                                                            console.log(err);
                                                        }
                                                    });
                                                });
                                            } else {
                                                console.log(err);
                                            }
                                        });
                                    });
                                } else {
                                    console.log(err);
                                }
                            });
                        });
                    } else {
                        console.log(err);
                    }
                });
            });
        });
    });
};

App.ui.prototype.tplaProducto = JSON.parse('[:TPLA_PRODUCTO:]');

App.ui.prototype.serializeTable = function () {
    var r = [];
    $('.pparams').each(function () {
        $(this).find('tbody tr').each(function () {
            var $this = $(this);
            var o = {};
            var i = $this.find('td:last-child input');
            o.id = i.attr('data-name').replace('idparam_', '');
            if (i.val() + "" !== "") {
                o.value = i.val();
            } else {
                o.value = 0;
            }
            r.push(o);
        });
    });
    return JSON.stringify(r);
};


App.reloadTable = function (callback) {
    App.InitVV.init(function () {
        App.core.loadApp("menu:recepcion_calidad");
    });
};

$(function () {
    App.UICalidad = new App.ui();
    App.UICalidad.init();
});
