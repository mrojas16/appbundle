/* global App, xmodal, blackout, JSONTRUE */
/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Pesaje.js
 * @desc        Gestion de Pesaje, formularios, tablas y animaciones
 *
 */


//App.idproducto = 0;
$("[data-tablemain]").dataTable();
$("#primal-form").validate({
    rules: {
    },
    messages: {
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (form) {
        var dataForm = $(form).serialize();
        var idpk = parseInt($(".pk_form").val());
        if (idpk === 0) {
            blackout.hide(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>No ha seleccionado correctamente la recepcion!</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 4000
                });
            });
        } else {
            blackout.show("Guardando...", function () {
                $.ajax({
                    type: "POST",
                    url: "[:__BASE_URL:]index.acopio/pesaje/edit",
                    data: dataForm
                }).done(function (data) {
                    try {
                        var r = JSON.parse(data);
                        setTimeout(function () {
                            if (r.response === JSONTRUE) {
                                blackout.hide(function () {
                                    App.oTable.fnDraw();
                                    App.xDom.closeForm();
                                    $.gritter.add({
                                        title: "OK!",
                                        text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                        class_name: 'growl-success',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                            }
                        }, 1);
                        $(".pk_form").val(0);
                    } catch (e) {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 2000
                            });
                        });
                    }
                }).error(function () {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                });
            });
        }
    }
});

/*
 App.core.loadMysql(function () {
 App.core.connection.query(q, function (err, get, fields) {
 */
App.pesar = function (id) {
    blackout.show('Espere...', function () {
        App.core.loadMysql(function () {
            var q = "SELECT * FROM fbr_v_recepciones WHERE idrecepcion = " + id + " LIMIT 1;";
            App.core.connection.query(q, function (err, data, fields) {
                if (!err) {
                    var $d = data[0];
                    App.ps.idproducto = $d.idproducto;
                    $('#idproducto').val($d.idproducto);
                    var $idproducto = $('#idproducto').val();
                    App.core.loadMysql(function () {
                        var q = "SELECT * FROM fbr_v_recepcion_pallet_pesajes WHERE idrecepcion = " + id + ";";
                        App.core.connection.query(q, function (err, table, fields) {
                            if (!err) {
                                var $table = table;
                                App.core.loadMysql(function () {
                                    var q = "SELECT * FROM fbr_v_pallets WHERE activo = 1 AND estado = 1 AND idacopio = " + App.GLOBAL.ACOPIO + " AND idproducto = " + $idproducto + " AND idtemporada = " + App.GLOBAL.TEMPORADA + ";";
                                    App.core.connection.query(q, function (err, inputs, fields) {
                                        if (!err) {
                                            var $inputs = '';
                                            for (var i = 0; i < inputs.length; i++) {
                                                var row = inputs[i];
                                                $inputs += "<option value='" + row.idpallet + "'>" + row.folio_i + ' - ' + row.producto + "</option>";
                                            }
                                            App.core.loadMysql(function () {
                                                var q = "SELECT * FROM fbr_v_pallets WHERE activo = 1 AND idacopio = " + App.GLOBAL.ACOPIO + " AND idproducto = " + $idproducto + " AND idtemporada = " + App.GLOBAL.TEMPORADA + ";";
                                                console.log(q);
                                                App.core.connection.query(q, function (err, pallets, fields) {
                                                    if (!err) {
                                                        var pad = '00000';
                                                        var $pallets = '';
                                                        for (var i = 0; i < pallets.length; i++) {
                                                            var $p = pallets[i];
                                                            if ($p.estado !== 2) {
                                                                $pallets += '<tr class="pallet" data-pallet="' + $p.idpallet + '" data-cajaspallet="' + $p.cajas_pallet + '">';
                                                                $pallets += '<td>' + (pad + $p.correlativo).slice(-pad.length) + '</td>';
                                                                $pallets += '<td>' + $p.producto + '</td>';
                                                                $pallets += '<td class="numbers cajas-cargadas-situ">' + $p.cajas + '</td>';
                                                                $pallets += '<td class="numbers cajas-full-pallet-situ">' + $p.cajas_pallet + '</td>';
                                                                $pallets += '</tr>';
                                                            }
                                                        }
                                                        blackout.hide(function () {
                                                            $('[data-block="folio_pallet"]').text('MP' + $d.correlativo_i);
                                                            $('[data-block="fecha_operacion"]').text($d.fecha_operacion_i);
                                                            $('[data-block="info_productor"]').text($d.productor);
                                                            $('.container-pallets').html($pallets);
                                                            App.ps.draw($table);
                                                            $("[data-maincontainerform]").slideDown();
                                                            $("#idrecepcion").val(id);
                                                            $('[name="idpallet"]').html($inputs).val(null).trigger('chosen:updated');
                                                            $('[name="idbandeja"]').val(null).trigger('chosen:updated');
                                                        });
                                                    } else {
                                                        blackout.hide(function () {
                                                            $.gritter.add({
                                                                title: "Error",
                                                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                                class_name: 'growl-danger',
                                                                sticky: false,
                                                                time: 2000
                                                            });
                                                            console.log(err);
                                                        });
                                                    }
                                                });
                                            });
                                        } else {
                                            blackout.hide(function () {
                                                $.gritter.add({
                                                    title: "Error",
                                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                    class_name: 'growl-danger',
                                                    sticky: false,
                                                    time: 2000
                                                });
                                                console.log(err);
                                            });
                                        }
                                    });
                                });
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                    console.log(err);
                                });
                            }
                        });
                    });
                } else {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                        console.log(err);
                    });
                }
            });
        });
    });
};


$('[name="peso_neto"],[name="idbandeja"],[name="cant_bandejas"]').bind('change keyup blur focus', function () {
    var $val = $('[name="peso_neto"]').val();
    var band = $('[name="idbandeja"]').val();
    var cant = $('[name="cant_bandejas"]').val();
    var $target = $('[name="peso_promedio_dest"]');

    App.core.loadMysql(function () {
        var q = "SELECT * FROM fbr_envase WHERE activo = 1 AND tipo = 1 AND idenvase = " + band + " LIMIT 1;";
        App.core.connection.query(q, function (err, env, fields) {
            if (!err) {
                var $a = env[0];
                if (band && band !== "" && cant && cant !== "") {
                    var pesob = $a.peso;
                    var pesod = (Math.round((($val - (cant * pesob)) / cant) * 100) / 100);
                    $target.val(pesod);
                } else {
                    $target.val(0);
                    console.log('pprom FAIL');
                }
            }
        });
    });
});


App.getBandejasInfo = function (callbacki) {
    App.core.loadMysql(function () {
        var q = 'SELECT codenvase,peso,peso_minimo,peso_maximo,idenvase FROM fbr_envase WHERE activo = 1 AND tipo = 1;';
        App.core.connection.query(q, function (err, rows, fields) {
            if (!err) {
                var r = [];
                for (var i = 0; i < rows.length; i++) {
                    var o = {};
                    o.codenvase = rows[i].codenvase;
                    o.peso = parseFloat(rows[i].peso);
                    o.peso_minimo = parseFloat(rows[i].peso_minimo);
                    o.peso_maximo = parseFloat(rows[i].peso_maximo);
                    r.push(o);
                }
                callbacki(r);
                App.ps.bandejas = r;
            }
        });
    });
};

App.pesaje = function () {
    this.idpallet = null;
    this.cajastotales = 0;
    this.palletContainer = $(".container-pallets");
    this.idproducto = 0;
    this.bandejas = null;
    var self = this;
    App.getBandejasInfo(function () {

    });

    this.run(function (i) {

        self.bandejas = i;
    });
};

App.pesaje.prototype.run = function (callbacki) {
    $("[data-addnewpallet]").bind('click', function (err) {
        blackout.show('Guardando....', function () {
            var id = ($(".pk_form").val());
            App.core.loadMysql(function () {
                var q = "SELECT * FROM fbr_recepcion_pallet WHERE activo = 1 ORDER BY folio DESC LIMIT 1";
                App.core.connection.query(q, function (err, row, fields) {
                    if (!err) {
                        var lastFolio = null;
                        var query = row[0];
                        if (query) {
                            lastFolio = (query.folio + 1);
                        } else {
                            lastFolio = 1;
                        }
                        App.core.loadMysql(function () {
                            var q = " SELECT * FROM fbr_recepcion WHERE idrecepcion = " + id + " LIMIT 1;";
                            App.core.connection.query(q, function (err, row, fields) {
                                if (!err) {
                                    var $producto = row[0].idproducto;
                                    App.core.loadMysql(function () {
                                        var q = "INSERT INTO fbr_recepcion_pallet (folio,idacopio,idproducto,idtemporada,correlativo,estado,fecha_operacion,fecha_creacion,idcreador,activo)\n\
                                                VALUES(\n\
                                                " + lastFolio + ",\n\
                                                " + App.GLOBAL.ACOPIO + ",\n\
                                                " + $producto + ",\n\
                                                " + App.GLOBAL.TEMPORADA + ",\n\
                                                " + lastFolio + ",\n\
                                                1,\n\
                                                NOW(),\n\
                                                NOW(),\n\
                                                " + App.GLOBAL.USUARIO + ",\n\
                                                1)";
                                        App.core.connection.query(q, function (err, row, fields) {
                                            if (!err) {
                                                var id = row.insertId;
                                                App.core.loadMysql(function () {
                                                    var q = "SELECT * FROM fbr_recepcion_pallet WHERE idpallet = " + id + " LIMIT 1;";
                                                    App.core.connection.query(q, function (err, row, fields) {
                                                        if (!err) {
                                                            var idproducto = row[0].idproducto;
                                                            App.core.loadMysql(function () {
                                                                var q = "SELECT * FROM fbr_v_pallets WHERE idacopio = " + App.GLOBAL.ACOPIO + " AND idproducto = " + idproducto + " AND idtemporada = " + App.GLOBAL.TEMPORADA + " AND estado = 1 AND activo = 1;";
                                                                App.core.connection.query(q, function (err, row, fields) {
                                                                    if (!err) {
                                                                        var $palA = row;
                                                                        App.core.loadMysql(function () {
                                                                            var q = "SELECT * FROM fbr_v_pallets WHERE idacopio = " + App.GLOBAL.ACOPIO + " AND idproducto = " + idproducto + " AND idtemporada = " + App.GLOBAL.TEMPORADA + " AND activo = 1;";
                                                                            App.core.connection.query(q, function (err, row, fields) {
                                                                                if (!err) {
                                                                                    var $pal = row;
                                                                                    var $r = '';
                                                                                    for (var i = 0; i < $pal.length; i++) {
                                                                                        var $p = $pal[i];
                                                                                        if ($p.estado !== 2) {
                                                                                            $r += '<tr class="pallet" data-pallet="' + $p.idpallet + '" data-cajaspallet="' + $p.cajas_pallet + '">';
                                                                                            $r += '<td>' + ($p.correlativo) + '</td>\n\
                                                                                                <td>' + $p.producto + '</td>\n\
                                                                                                <td class="numbers cajas-cargadas-situ">' + $p.cajas + '</td>\n\
                                                                                                <td class="numbers cajas-full-pallet-situ">' + $p.cajas_pallet + '</td>\n\
                                                                                                </tr>';
                                                                                        }
                                                                                    }
                                                                                    var $pallets = $r;
                                                                                    var $in = '';
                                                                                    for (var i = 0; i < $palA.length; i++) {
                                                                                        var row = $palA[i];
                                                                                        $in += "<option value='" + row.idpallet + "'>" + row.folio_i + ' - ' + row.producto + "</option>";
                                                                                    }
                                                                                    var $inputs = $in;
                                                                                    blackout.hide(function () {
                                                                                        $('.container-pallets').html($pallets);
                                                                                        $.gritter.add({
                                                                                            title: "OK!",
                                                                                            text: "<i class='fa fa-clock-o'></i> <i>Se ha Creado Correctamente un pallet</i>",
                                                                                            class_name: 'growl-success',
                                                                                            sticky: false,
                                                                                            time: 2000
                                                                                        });
                                                                                    });
                                                                                    App.getBandejasInfo(callbacki);
                                                                                } else {
                                                                                    blackout.hide(function () {
                                                                                        $.gritter.add({
                                                                                            title: "Error",
                                                                                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                                                            class_name: 'growl-danger',
                                                                                            sticky: false,
                                                                                            time: 4000
                                                                                        });
                                                                                        console.log(err);
                                                                                    });
                                                                                }
                                                                            });
                                                                        });
                                                                    } else {
                                                                        blackout.hide(function () {
                                                                            $.gritter.add({
                                                                                title: "Error",
                                                                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                                                class_name: 'growl-danger',
                                                                                sticky: false,
                                                                                time: 4000
                                                                            });
                                                                            console.log(err);
                                                                        });
                                                                    }
                                                                });
                                                            });
                                                        } else {
                                                            blackout.hide(function () {
                                                                $.gritter.add({
                                                                    title: "Error",
                                                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                                    class_name: 'growl-danger',
                                                                    sticky: false,
                                                                    time: 4000
                                                                });
                                                                console.log(err);
                                                            });
                                                        }
                                                    });
                                                });
                                            } else {
                                                blackout.hide(function () {
                                                    $.gritter.add({
                                                        title: "Error",
                                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                        class_name: 'growl-danger',
                                                        sticky: false,
                                                        time: 4000
                                                    });
                                                    console.log(err);
                                                });
                                            }
                                        });
                                    });
                                } else {
                                    blackout.hide(function () {
                                        $.gritter.add({
                                            title: "Error",
                                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                            class_name: 'growl-danger',
                                            sticky: false,
                                            time: 4000
                                        });
                                        console.log(err);
                                    });
                                }
                            });
                        });
                    } else {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 4000
                            });
                            console.log(err);
                        });
                    }
                });
            });
        });
    });

    $('[data-addbandeja]').bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var band = $('[name=idbandeja]').val();
        var idrec = $('[name=idrecepcion]').val();
        var pallet = $(".active-pallet").attr('data-pallet');
        var cant = $('[name=cant_bandejas]').val();
        var peso, prom;
        var $target = $('.list_pesaje');
        var idproducto = App.ps.idproducto;
        var $res = '';
        var totalcargadassitu = parseInt($(".active-pallet .cajas-cargadas-situ").text());
        var cajastotalessitu = parseInt($(".active-pallet .cajas-full-pallet-situ").text());
        var disp = cajastotalessitu - totalcargadassitu;
        var verifyMax = function () {
            var $val = $('[name="peso_neto"]').val();
            var band = $('[name="idbandeja"]').val();
            var cant = $('[name="cant_bandejas"]').val();
            if (band && band !== "" && cant && cant !== "") {
                var pesob = App.ps.bandejas[band].peso;
                peso = ($val - (cant * pesob));
                var pesod = prom = ($val - (cant * pesob)) / cant;
                if (pesod > App.ps.bandejas[band].peso_maximo) {
                    return false;
                }
            }
            return true;
        };
        if (!verifyMax()) {
            $.gritter.add({
                title: "Error",
                text: "<i class='fa fa-clock-o'></i> <i>El peso promedio de las cajas supera al máximo</i>",
                class_name: 'growl-danger',
                sticky: false,
                time: 3000
            });
//            return false;
        } else
        if (parseFloat($('[name="peso_promedio_dest"]').val()) < 0) {
            $.gritter.add({
                title: "Error",
                text: "<i class='fa fa-clock-o'></i> <i>El peso promedio es inferior a 0</i>",
                class_name: 'growl-danger',
                sticky: false,
                time: 3000
            });
//            return false;
        } else if (cant > disp) {
            $.gritter.add({
                title: "Error",
                text: "<i class='fa fa-clock-o'></i> <i>La cantidad de cajas maximas aignables al pallet es de <b>" + disp + "</b>, pruebe con menos cajas, o un pallet distinto</i>",
                class_name: 'growl-danger',
                sticky: false,
                time: 3000
            });
//            return false;
        } else {
            if (typeof pallet !== "undefined" && pallet !== "") {
                if (band === "" || cant === "" || peso === "") {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>Debe llenar todos los datos de pesaje para poder ingresarlo</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                } else {
                    blackout.show('Espere...', function () {
                        App.core.loadMysql(function () {
                            var q = "SELECT * FROM fbr_recepcion WHERE idrecepcion = " + idrec + " LIMIT 1;";
                            App.core.connection.query(q, function (err, row, fields) {
                                if (!err) {
                                    var $idrecepcion = row[0].idrecepcion;
                                    var idenvase = band;
                                    var cant_bandejas = cant;
                                    var kilos_netos = peso;
                                    var idpallet = (pallet);
                                    App.core.loadMysql(function () {
                                        var q = "INSERT INTO fbr_recepcion_pesaje (idrecepcion,cant_bandejas,kilos_netos,idenvase)\n\
                                            VALUES(" + $idrecepcion + ",\n\
                                                    " + cant_bandejas + ",\n\
                                                    " + kilos_netos + ",\n\
                                                    " + idenvase + ");";
                                        App.core.connection.query(q, function (err, row, fields) {
                                            if (!err) {
                                                var pkp = row.insertId;
                                                App.core.loadMysql(function () {
                                                    var q = 'INSERT INTO fbr_recepcion_pallet_pesaje (idpallet,idpesaje)\n\
                                                        VALUES(' + idpallet + ',' + pkp + ')';
                                                    App.core.connection.query(q, function (err, row, fields) {
                                                        if (!err) {
                                                            App.core.loadMysql(function () {
                                                                var q = "SELECT * FROM fbr_v_pallets WHERE idacopio = " + App.GLOBAL.ACOPIO + " AND idproducto = " + idproducto + " AND idtemporada = " + App.GLOBAL.TEMPORADA + " AND estado = 1 AND activo = 1;";
                                                                App.core.connection.query(q, function (err, row, fields) {
                                                                    if (!err) {
                                                                        var $palA = row;
                                                                        App.core.loadMysql(function () {
                                                                            var q = "SELECT * FROM fbr_v_pallets WHERE idacopio = " + App.GLOBAL.ACOPIO + " AND idproducto = " + idproducto + " AND idtemporada = " + App.GLOBAL.TEMPORADA + " AND activo = 1;";
                                                                            console.log(q);
                                                                            App.core.connection.query(q, function (err, table, fields) {
                                                                                if (!err) {
                                                                                    var $pal = table;
                                                                                    var $r = '';
                                                                                    for (var i = 0; i < $pal.length; i++) {
                                                                                        var $p = $pal[i];
                                                                                        if ($p.estado !== 2) {
                                                                                            $r += '<tr class="pallet" data-pallet="' + $p.idpallet + '" data-cajaspallet="' + $p.cajas_pallet + '">';
                                                                                            $r += ' <td>' + ($p.correlativo) + '</td>\n\
                                                                                        <td>' + $p.producto + '</td>\n\
                                                                                        <td class="numbers cajas-cargadas-situ">' + $p.cajas + '</td>\n\
                                                                                        <td class="numbers cajas-full-pallet-situ">' + $p.cajas_pallet + '</td>\n\
                                                                                   </tr>';
                                                                                        }
                                                                                    }
                                                                                    console.log($r);
                                                                                    var $pallets = $r;
                                                                                    var $in = '';
                                                                                    for (var i = 0; i < $palA.length; i++) {
                                                                                        var row = $palA[i];
                                                                                        $in += "<option value='" + row.idpallet + "'>" + row.folio_i + ' - ' + row.producto + "</option>";
                                                                                    }
                                                                                    var $inputs = $in;
                                                                                    var $res = '';
                                                                                    $('.container-pallets').html($pallets);
                                                                                    blackout.hide(function () {
                                                                                        $res += '<tr data-pesajepk="' + pkp + '" data-palletdep="' + pallet + '">';
                                                                                        $res += '<td data-value="' + band + '" data-name="idbandeja">' + $('[name=idbandeja] option[value="' + band + '"]').text() + '</td>';
                                                                                        $res += '<td data-value="' + cant + '" data-name="cant_bandeja" data-typenumber>' + cant + '</td>';
                                                                                        $res += '<td data-value="' + ((Math.round(prom * 100)) / 100) + '" data-name="peso_prom" data-typenumber>' + ((Math.round(prom * 100)) / 100) + '</td>';
                                                                                        $res += '<td data-value="' + ((Math.round(peso * 100)) / 100) + '" data-name="peso" data-typenumber>' + ((Math.round(peso * 100)) / 100) + '</td>';
                                                                                        $res += '<td><a class="btn btn-xs btn-primary btn-danger" onclick="App.ps.delete(\'' + pkp + '\')"><i class="fa fa-delete"></i> Eliminar</a></td>';
                                                                                        $res += '</tr>';
                                                                                        if ($target.find('tbody tr.nodata-msg').length) {
                                                                                            $target.find('tbody').html($res);
                                                                                        } else {
                                                                                            $target.find('tbody').append($res);
                                                                                        }
                                                                                        App.ps.selectPallet($('tr[data-pallet="' + $('[name="idpallet"]').val() + '"]'));

                                                                                    });
                                                                                } else {
                                                                                    blackout.hide(function () {
                                                                                        $.gritter.add({
                                                                                            title: "Error",
                                                                                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                                                            class_name: 'growl-danger',
                                                                                            sticky: false,
                                                                                            time: 2000
                                                                                        });
                                                                                        console.log(err);
                                                                                    });
                                                                                }
                                                                            });
                                                                        });
                                                                    } else {
                                                                        blackout.hide(function () {
                                                                            $.gritter.add({
                                                                                title: "Error",
                                                                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                                                class_name: 'growl-danger',
                                                                                sticky: false,
                                                                                time: 2000
                                                                            });
                                                                            console.log(err);
                                                                        });
                                                                    }
                                                                });
                                                            });
                                                        } else {
                                                            blackout.hide(function () {
                                                                $.gritter.add({
                                                                    title: "Error",
                                                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                                                    class_name: 'growl-danger',
                                                                    sticky: false,
                                                                    time: 2000
                                                                });
                                                                console.log(err);
                                                            });
                                                        }
                                                    });
                                                });

                                            } else {
                                                blackout.hide(function () {
                                                    $.gritter.add({
                                                        title: "Error",
                                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                                        class_name: 'growl-danger',
                                                        sticky: false,
                                                        time: 2000
                                                    });
                                                    console.log(err);
                                                });
                                            }
                                        });
                                    });
                                } else {
                                    blackout.hide(function () {
                                        $.gritter.add({
                                            title: "Error",
                                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                            class_name: 'growl-danger',
                                            sticky: false,
                                            time: 2000
                                        });
                                        console.log(err);
                                    });
                                }
                            });
                        });
                    });
                }
            } else {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>Debe Seleccionar un pallet de destino</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
            }
        }
    });
};

App.pesaje.prototype.launch = function () {
};
App.pesaje.prototype.openPallet = function (obj) {
    blackout.show('Cargando...', function () {
        var $this = $(obj);
        var id = atob($this.attr('data-abrirpallet'));
        App.core.loadMysql(function () {
            var q = 'UPDATE fbr_recepcion_pallet\n\
                SET estado = 1\n\
                WHERE idpallet = ' + id + ';';
            App.core.connection.query(q, function (err, row, fields) {
                if (!err) {
                    App.core.loadMysql(function () {
                        var q = 'SELECT * FROM fbr_recepcion WHERE idrecepcion = ' + ($(".pk_form").val()) + ' LIMIT 1;';
                        App.core.connection.query(q, function (err, row, fields) {
                            if (!err) {
                                var $idproducto = row[0].idproducto;
                                App.core.loadMysql(function () {
                                    var q = 'SELECT * FROM fbr_v_pallets WHERE idacopio = ' + App.GLOBAL.ACOPIO + ' AND idproducto = ' + $idproducto + ' AND idtemporada = ' + App.GLOBAL.TEMPORADA + ' AND estado = 1 AND activo = 1;';
                                    App.core.connection.query(q, function (err, row, fields) {
                                        if (!err) {
                                            var $palA = row;
                                            App.core.loadMysql(function () {
                                                var q = 'SELECT * FROM fbr_v_pallets WHERE idacopio = ' + App.GLOBAL.ACOPIO + ' AND idproducto = ' + $idproducto + ' AND idtemporada = ' + App.GLOBAL.TEMPORADA + ' AND activo = 1;';
                                                App.core.connection.query(q, function (err, row, fields) {
                                                    if (!err) {
                                                        var $pal = row;
                                                        var $r = '';
                                                        var $response = 0;
                                                        for (var i = 0; i < $pal.length; i++) {
                                                            var $p = $pal[i];
                                                            if ($p.estado !== 2) {
                                                                if ($p.estado !== 2) {
                                                                    $r += '<tr class="pallet" data-pallet="' + $p.idpallet + '" data-cajaspallet="' + $p.cajas_pallet + '">';
                                                                    $r += ' <td>' + ($p.correlativo) + '</td>\n\
                                                                                        <td>' + $p.producto + '</td>\n\
                                                                                        <td class="numbers cajas-cargadas-situ">' + $p.cajas + '</td>\n\
                                                                                        <td class="numbers cajas-full-pallet-situ">' + $p.cajas_pallet + '</td>\n\
                                                                                   </tr>';
                                                                }
                                                            }
                                                            $response = ($response + 1);
                                                        }

                                                        var $pallets = $r;
                                                        var $in = '';
                                                        for (var i = 0; i < $palA.length; i++) {
                                                            var row = $palA[i];
                                                            $in += "<option value='" + row.idpallet + "'>" + row.folio_i + ' - ' + row.producto + "</option>";
                                                        }
                                                        if ($response !== 0) {
                                                            $('.container-pallets').html($pallets);
                                                            $('table.list_pesaje [data-pesajepk="' + id + '"]').slideUp(function () {
                                                                $('table.list_esaje [data-pesajepk="' + id + '"]').remove();
                                                            });
                                                            App.ps.launch();
                                                            blackout.hide(function () {
                                                                $.gritter.add({
                                                                    title: "OK!",
                                                                    text: "<i class='fa fa-clock-o'></i> <i>Se ha reabierto correctamente el pallet</i>",
                                                                    class_name: 'growl-success',
                                                                    sticky: false,
                                                                    time: 2000
                                                                });
                                                            });
                                                        } else {
                                                            blackout.hide(function () {
                                                                $.gritter.add({
                                                                    title: "Error",
                                                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                                    class_name: 'growl-danger',
                                                                    sticky: false,
                                                                    time: 2000
                                                                });
                                                            });
                                                        }
                                                    } else {
                                                        blackout.hide(function () {
                                                            $.gritter.add({
                                                                title: "Error",
                                                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                                class_name: 'growl-danger',
                                                                sticky: false,
                                                                time: 2000
                                                            });
                                                        });
                                                        console.log(err);
                                                    }
                                                });
                                            });
                                        } else {
                                            blackout.hide(function () {
                                                $.gritter.add({
                                                    title: "Error",
                                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                    class_name: 'growl-danger',
                                                    sticky: false,
                                                    time: 2000
                                                });
                                            });
                                            console.log(err);
                                        }
                                    });
                                });
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 2000
                                    });
                                });
                                console.log(err);
                            }
                        });
                    });
                } else {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    });
                    console.log(err);
                }
            });
        });
    });
};
App.pesaje.prototype.lastSelectedPallet = null;
App.pesaje.prototype.selectPallet = function (obj) {
    var o = $(obj);
    App.pesaje.prototype.lastSelectedPallet = o.attr('data-pallet');
    this.palletContainer.find('.active-pallet').removeClass('active-pallet');
    this.idpallet = o.attr('data-pallet');
    this.cajastotales = o.attr('data-cajaspallet');
    o.addClass('active-pallet');
};
App.pesaje.prototype.verifyEliminable = function (id) {
    var t = $('tr[data-pesajepk="' + id + '"]').attr("data-palletdep");
    var x = $("[data-pallet='" + (t) + "']");
    if (x.length > 0) {
        return true;
    }
    return false;
};

App.pesaje.prototype.delete = function (id) {
    if (this.verifyEliminable(id)) {
        App.core.loadMysql(function () {
            var q = 'DELETE FROM fbr_recepcion_pallet_pesaje WHERE idpesaje = ' + id + ';';
            App.core.connection.query(q, function (err, get, fields) {
                if (!err) {
                    App.core.loadMysql(function () {
                        var q = 'DELETE FROM fbr_recepcion_pesaje WHERE  idpesaje = ' + id + ';';
                        App.core.connection.query(q, function (err, get, fields) {
                            if (!err) {
                                App.core.loadMysql(function () {
                                    var q = 'SELECT * FROM fbr_recepcion WHERE idrecepcion = ' + ($(".pk_form").val()) + ' LIMIT 1;';
                                    App.core.connection.query(q, function (err, row, fields) {
                                        if (!err) {
                                            var $idproducto = row[0].idproducto;
                                            App.core.loadMysql(function () {
                                                var q = 'SELECT * FROM fbr_v_pallets WHERE idacopio = ' + App.GLOBAL.ACOPIO + ' AND idproducto = ' + $idproducto + ' AND idtemporada = ' + App.GLOBAL.TEMPORADA + ' AND estado = 1 AND activo = 1;';
                                                App.core.connection.query(q, function (err, row, fields) {
                                                    if (!err) {
                                                        var $palA = row;
                                                        App.core.loadMysql(function () {
                                                            var q = 'SELECT * FROM fbr_v_pallets WHERE idacopio = ' + App.GLOBAL.ACOPIO + ' AND idproducto = ' + $idproducto + ' AND idtemporada = ' + App.GLOBAL.TEMPORADA + ' AND activo = 1;';
                                                            App.core.connection.query(q, function (err, row, fields) {
                                                                if (!err) {
                                                                    var $pal = row;
                                                                    var $r = '';
                                                                    var $response = 0;
                                                                    for (var i = 0; i < $pal.length; i++) {
                                                                        var $p = $pal[i];
                                                                        if ($p.estado !== 2) {
                                                                            if ($p.estado !== 2) {
                                                                                $r += '<tr class="pallet" data-pallet="' + $p.idpallet + '" data-cajaspallet="' + $p.cajas_pallet + '">';
                                                                                $r += ' <td>' + ($p.correlativo) + '</td>\n\
                                                                                        <td>' + $p.producto + '</td>\n\
                                                                                        <td class="numbers cajas-cargadas-situ">' + $p.cajas + '</td>\n\
                                                                                        <td class="numbers cajas-full-pallet-situ">' + $p.cajas_pallet + '</td>\n\
                                                                                   </tr>';
                                                                            }
                                                                        }
                                                                        $response = ($response + 1);
                                                                    }
                                                                    var $pallets = $r;
                                                                    var $in = '';
                                                                    for (var i = 0; i < $palA.length; i++) {
                                                                        var row = $palA[i];
                                                                        $in += "<option value='" + row.idpallet + "'>" + row.folio_i + ' - ' + row.producto + "</option>";
                                                                    }
                                                                    if ($response !== 0) {
                                                                        $('.container-pallets').html($pallets);
                                                                        console.log('table.list_pesaje [data-pesajepk="' + id + '"]');
                                                                        $('table.list_pesaje [data-pesajepk="' + id + '"]').slideUp(function () {
                                                                            $('table.list_esaje [data-pesajepk="' + id + '"]').remove();
                                                                        });
                                                                        App.ps.launch();
                                                                        blackout.hide(function () {
                                                                            $.gritter.add({
                                                                                title: "OK!",
                                                                                text: "<i class='fa fa-clock-o'></i> <i>Se ha eliminado Correctamente el pesaje</i>",
                                                                                class_name: 'growl-success',
                                                                                sticky: false,
                                                                                time: 2000
                                                                            });
                                                                            App.ps.selectPallet($('tr[data-pallet="' + $('[name="idpallet"]').val() + '"]'));
                                                                        });
                                                                    } else {
                                                                        blackout.hide(function () {
                                                                            $.gritter.add({
                                                                                title: "Error",
                                                                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                                                class_name: 'growl-danger',
                                                                                sticky: false,
                                                                                time: 2000
                                                                            });
                                                                        });
                                                                    }
                                                                } else {
                                                                    blackout.hide(function () {
                                                                        $.gritter.add({
                                                                            title: "Error",
                                                                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                                            class_name: 'growl-danger',
                                                                            sticky: false,
                                                                            time: 2000
                                                                        });
                                                                    });
                                                                    console.log(err);
                                                                }
                                                            });
                                                        });
                                                    } else {
                                                        blackout.hide(function () {
                                                            $.gritter.add({
                                                                title: "Error",
                                                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error rescatando los datos</i>",
                                                                class_name: 'growl-danger',
                                                                sticky: false,
                                                                time: 2000
                                                            });
                                                        });
                                                        console.log(err);
                                                    }
                                                });
                                            });
                                        } else {
                                            console.log(err);
                                        }
                                    });
                                });
                            } else {
                                console.log(err);
                            }
                        });
                    });
                } else {
                    console.log(err);
                }
            });
        });
    } else {

    }
};

App.pesaje.prototype.draw = function (tab) {
    var $res = '';
    for (var i = 0; i < tab.length; i++) {
        var a = tab[i];
        var prom = (Math.round((a.kilos_netos / a.cant_bandejas) * 100) / 100);
        var kn = (Math.round((a.kilos_netos) * 100) / 100);
        $res += '<tr data-pesajepk="' + a.idpesaje + '" data-palletdep="' + a.idpallet + '">';
        $res += '<td data-value="' + a.idenvase + '" data-name="idbandeja">' + $('[name=idbandeja] option[value="' + a.idenvase + '"]').text() + '</td>';
        $res += '<td data-value="' + a.cant_bandejas + '" data-name="cant_bandeja" data-typenumber>' + a.cant_bandejas + '</td>';
        $res += '<td data-value="' + prom + '" data-name="peso_prom" data-typenumber>' + prom + '</td>';
        $res += '<td data-value="' + kn + '" data-name="peso" data-typenumber>' + kn + '</td>';
        $res += '<td><a class="btn btn-xs btn-primary btn-danger" onclick="App.ps.delete(\'' + a.idpesaje + '\')"><i class="fa fa-delete"></i> Eliminar</a></td>';
        $res += '</tr>';
    }
    $("table.list_pesaje tbody").html($res);
};



App.pesaje.prototype.init = function () {
    $('[name="idpallet"]').val('').trigger('chosen:updated');
    $('[name="peso_neto"],[name="idbandeja"],[name="cant_bandejas"]').bind('change keyup blur focus', function () {
        var $val = $('[name="peso_neto"]').val();
        var band = $('[name="idbandeja"]').val();
        var cant = $('[name="cant_bandejas"]').val();
        var $target = $('[name="peso_promedio_dest"]');
        if (band && band !== "" && cant && cant !== "") {
            var pesob = App.ps.bandejas[band].peso;
            var pesod = (Math.round((($val - (cant * pesob)) / cant) * 100) / 100);
            $target.val(pesod);
        } else {
            $target.val(0);
            console.log('pprom FAIL');
        }
    });
    $('[name="idpallet"]').bind('change', function () {
        var $this = $(this);
        var bv = $this.val();
        App.ps.selectPallet($('tr[data-pallet="' + bv + '"]'));
    });
    if (parseInt("[:MOD_PESAJE:]") === 2) {
        this.activatePesaje();
    }
};
App.pesaje.prototype.activatePesaje = function () {
    var $target = $('[name="peso_neto"]');
    var $error = $("#CommunicationError");
    var extensionId = "[:EXTENSION_ID:]";
    setInterval(function () {
        chrome.runtime.sendMessage(extensionId, {getData: true, receiver: $target},
                function (response) {
                    try {
                        if (response.status) {
                            var r = (Math.round(parseFloat(response.response) * 100)) / 100;
                            $target.prop('readonly', true)
                                    .val(r).change();
                            $error.slideUp();
                        } else {
                            $target.prop('readonly', false);
                            $error.slideDown();
                        }
                    } catch (e) {
                        $target.prop('readonly', false);
                        $error.slideDown();
                    }
                }
        );
    }, 100);
};
App.ps = new App.pesaje();
App.ps.init();
