/*
 App.core.loadMysql(function () {
 App.core.connection.query(q, function (err, corr, fields) {
 */

$("[data-addnew]").bind('click', function () {
});
$("[data-tablemain]").dataTable();
$("#primal-form").validate({
    rules: {
        idproducto: {
            required: true
        }
    },
    messages: {
        idproducto: {
            required: "Debe ingresar un(a) idproducto"
        }
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (form) {
        var dataForm = $(form).serialize();
        var idpk = parseInt($(".pk_form").val());
        if (idpk === 0 || isNaN(idpk)) {
            blackout.show("Guardando...", function () {
                App.core.loadMysql(function () {
                    var q = "SELECT * FROM fbr_recepcion_pallet\n\
                                WHERE activo = 1\n\
                                ORDER BY folio DESC LIMIT 1";
                    App.core.connection.query(q, function (err, corr, fields) {
                        if (!err) {
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1;
                            var yyyy = today.getFullYear();
                            if (dd < 10) {
                                dd = '0' + dd
                            }

                            if (mm < 10) {
                                mm = '0' + mm
                            }

                            today = yyyy + '-' + mm + '-' + dd;
                            var correlativo = (corr.length > 0) ? (corr[0].folio + 1) : 1;

                            App.core.loadMysql(function () {
                                var q = "INSERT INTO fbr_recepcion_pallet (activo,estado,despachado,idproducto,peso_base,idcreador,fecha_creacion,idtemporada,idacopio,folio,correlativo) \n\
                                                VALUES(\n\
                                                        1,\n\
                                                        1,\n\
                                                         0,\n\
                                                        " + $('[name=idproducto]').val() + ",\n\
                                                        " + $('[name=peso_base]').val() + ",\n\
                                                        " + App.GLOBAL.USUARIO + ",\n\
                                                        '" + today + "', \n\
                                                        " + App.GLOBAL.TEMPORADA + ", \n\
                                                        " + App.GLOBAL.ACOPIO + ", \n\
                                                        '" + (correlativo) + "', \n\
                                                        '" + (correlativo) + "'); ";
                                App.core.connection.query(q, function (err, ps, fields) {
                                    if (!err) {
                                        App.reloadTable();
                                        blackout.hide(function () {
                                            $.gritter.add({
                                                title: "OK!",
                                                text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                                class_name: 'growl-success',
                                                sticky: false,
                                                time: 2000
                                            });
                                            callback();
                                        });
                                    } else {
                                        blackout.hide();
                                        console.log(q);
                                        console.log(err);
                                    }
                                });
                            });
                        } else {
                            blackout.hide();
                            callback();
                            console.log(err);
                        }
                    });
                });
            });
        } else {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = yyyy + '-' + mm + '-' + dd;
            blackout.show("Guardando...", function () {
                App.core.loadMysql(function () {
                    var q = "UPDATE fbr_recepcion_pallet\n\
                                SET \n\
                                idproducto = " + $('[name=idproducto]').val() + ",\n\
                                peso_base = " + $('[name=peso_base]').val() + ",\n\
                                fecha_edicion = " + today + ",\n\
                                idmodificador = " + App.GLOBAL.USUARIO + ";";
                    App.core.connection.query(q, function (err, ps, fields) {
                        if (!err) {
                            App.reloadTable();
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "OK!",
                                    text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                    class_name: 'growl-success',
                                    sticky: false,
                                    time: 2000
                                });
                            });
                        } else {
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "Error",
                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                                    class_name: 'growl-danger',
                                    sticky: false,
                                    time: 4000
                                });
                            });
                        }
                    });
                });

            });
        }
    }
});
App.editar = function (id) {
    blackout.show("Cargando...", function () {
        App.core.loadMysql(function () {
            var q = "SELECT * FROM fbr_recepcion_pallet\n\
                    WHERE idpallet = " + id + ";";
            App.core.connection.query(q, function (err, get, fields) {
                if (!err) {
                    $("[data-maincontainerform]").slideUp();
                    blackout.hide(function () {
                        $('#idpallet').val(get[0].idpallet);
                        $("[name=idproducto]").val(get[0].idproducto).trigger('chosen:updated');
                        $("[name=peso_base]").val(get[0].peso_base);
                        $("[data-maincontainerform]").slideDown();
                    });
                } else {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error cargando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                }
            });
        });
    });
};
App.eliminar = function (id) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    blackout.show("Eliminando...", function () {
        App.core.loadMysql(function () {
            var q = "UPDATE fbr_recepcion_pallet\n\
                    SET\n\
                    activo = 0,\n\
                    ideliminador = " + App.GLOBAL.USUARIO + ",\n\
                    fecha_eliminacion = '" + today + "'\n\
                WHERE idpallet = " + id + ";";
            console.log(q);
            App.core.connection.query(q, function (err, upt, fields) {
                if (!err) {
                    App.reloadTable();
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "OK!",
                            text: "<i class='fa fa-clock-o'></i> <i>Se han eliminado correctamente los datos</i>",
                            class_name: 'growl-success',
                            sticky: false,
                            time: 2000
                        });
                    });
                } else {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error eliminando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                }
            });
        });
    });
};
App.cerrar = function (id) {
    blackout.show("Cerrando...", function () {
        App.core.loadMysql(function () {
            var q = "SELECT * FROM fbr_recepcion_pallet_pesaje\n\
                    WHERE idpallet = " + id + ";";
            App.core.connection.query(q, function (err, row, fields) {
                if (!err) {
                    var $in = "0";
                    for (var i = 0; i < row.length; i++) {
                        $in += ',' + row[i].idpesaje;
                    }
//                    console.log(err);
//                    blackout.hide(function () {
                    App.core.loadMysql(function () {
                        var q = "SELECT * FROM fbr_recepcion_pesaje\n\
                                WHERE idpesaje in (" + $in + ");";
                        App.core.connection.query(q, function (err, row, fields) {
                            if (!err) {
                                var $allBand = 0;
                                var o = {};
                                var $qaBand = [];
                                for (var i = 0; i < row.length; i++) {
                                    var $pesaje = row[i];
                                    $allBand += o.cant_bandejas = $pesaje.cant_bandejas;
                                    App.core.loadMysql(function () {
                                        var q = "SELECT * FROM fbr_recepcion WHERE idrecepcion = " + $pesaje.idrecepcion + " LIMIT 1;";
                                        App.core.connection.query(q, function (err, row, fields) {
                                            if (!err) {
                                                o.qa_proyeccion = row[0].res_iqf;
                                            }
                                            $qaBand.push(o);
                                            var $ppe = 0;
                                            for (var i = 0; i < $qaBand.length; i++) {
                                                var $ab = $qaBand[i];
                                                $ppe += ($ab.cant_bandejas / $allBand) * $ab.qa_proyeccion;
                                            }
                                            App.core.loadMysql(function () {
                                                var q = "UPDATE fbr_recepcion_pallet\n\
                                                        SET\n\
                                                        estado = 2,\n\
                                                        calidad_ponderada = " + $ppe + "\
                                                        WHERE idpallet = " + id + " ;";
                                                App.core.connection.query(q, function (err, close, fields) {
                                                    if (!err) {
                                                        App.reloadTable();
                                                        blackout.hide(function () {
                                                            $.gritter.add({
                                                                title: "Ok",
                                                                text: "<i class='fa fa-clock-o'></i> <i>El estado se ha cerrado con exito</i>",
                                                                class_name: 'growl-success',
                                                                sticky: false,
                                                                time: 4000
                                                            });
                                                        });
                                                    } else {
                                                        blackout.hide(function () {
                                                            $.gritter.add({
                                                                title: "Error",
                                                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error eliminando los datos</i>",
                                                                class_name: 'growl-danger',
                                                                sticky: false,
                                                                time: 4000
                                                            });
                                                        });
                                                        console.log(err);
                                                    }
                                                });
                                            });
                                        });
                                    });
                                }
                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error solicitando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 4000
                                    });
                                });
                            }
                        });
                    });
//                    });
                }
            });
        });
    });
};
App.abrir = function (id) {
    blackout.show('Abriendo....', function () {
        App.core.loadMysql(function () {
            var q = "UPDATE fbr_recepcion_pallet\n\
                SET\n\
                estado = 1\n\
                WHERE idpallet = " + id + ";";
            console.log(q);
            App.core.connection.query(q, function (err, open, fields) {
                if (!err) {
                    App.reloadTable();
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Ok",
                            text: "<i class='fa fa-clock-o'></i> <i>El estado se ha cerrado con exito</i>",
                            class_name: 'growl-success',
                            sticky: false,
                            time: 4000
                        });
                    });
                }
            });
        });
    });
};
 App.guia = function (idpk){
     
 };

App.reloadTable = function (callback) {
    App.InitVV.init(function () {
        App.core.loadApp("menu:recepcion_pallets");
    });
};