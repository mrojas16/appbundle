/* global App, blackout, xmodal */

/**
 * -----------------------------------------------------------------------------
 * XWork Framework
 * -----------------------------------------------------------------------------
 * @author      Marcel Rojas Abarca <marcelrojas16@gmail.com>
 * @github      https://github.com/mrojas16
 * @package     XWORK
 * @version     1.0.9 v7
 *
 * @copyright   (c) 2014-2015 Marcel Rojas Abarca <http://github.com/mrojas16>
 * @license     [pordefinir]
 * -----------------------------------------------------------------------------
 *
 * @name        Despacho.js
 * @desc        Gestion de Despacho, formularios, tablas y animaciones
 *
 */


$("[data-tablemain]").dataTable();
$("#primal-form").validate({
    rules: {
        fecha_operacion: {
            required: true
        },
        hora_despacho: {
            required: true
        },
        num_guia: {
            required: true,
//            noRepeat: true
        },
        patente: {
            required: true
        },
        sello_1: {
            required: true
        },
        sello_2: {
            required: true
        },
        nombre_conductor: {
            required: true
        }
    },
    messages: {
        fecha_operacion: {
            required: "Debe ingresar fecha de operación"
        },
        hora_despacho: {
            required: "Debe ingresar hora de despacho"
        },
        num_guia: {
            required: "Debe ingresar número de guía",
//            noRepeat: "Este número de guía ya ha sido ingresado"
        },
        patente: {
            required: "Debe ingresar patente"
        },
        sello_1: {
            required: "Debe ingresar el primer sello del camion"
        },
        sello_2: {
            required: "Debe ingresar el segundo sello del camion"
        },
        nombre_conductor: {
            required: "Debe ingresar nombre del conductor"
        }
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (form) {
        var idpk = parseInt($(".pk_form").val());
        if (idpk === 0) {
            blackout.show('Guardando', function () {
                App.core.loadMysql(function () {
                    var q = 'INSERT INTO fbr_despacho(\n\
                        idtemporada,\n\
                        idacopio,\n\
                        fecha_operacion,\n\
                        hora_despacho,\n\
                        num_guia,\n\
                        patente,\n\
                        nombre_conductor,\n\
                        sello_1,\n\
                        sello_2,\n\
                        estado,\n\
                        recepcionado,\n\
                        activo,\n\
                        fecha_creacion,\n\
                        idcreador)\n\
                        VALUES(\n\
                        ' + App.GLOBAL.TEMPORADA + ',\n\
                        ' + App.GLOBAL.ACOPIO + ',\n\
                        "' + $('[name=fecha_operacion]').val() + '",\n\
                        "' + $('[name=hora_despacho]').val() + '",\n\
                        "' + $('[name=num_guia]').val() + '",\n\
                        "' + $('[name=patente]').val() + '",\n\
                        "' + $('[name=nombre_conductor]').val() + '",\n\
                        "' + $('[name=sello_1]').val() + '",\n\
                        "' + $('[name=sello_2]').val() + '",\n\
                        0,\n\
                        0,\n\
                        1,\n\
                        NOW(),\n\
                        ' + App.GLOBAL.USUARIO + ');';
                    App.core.connection.query(q, function (err, query, fields) {
                        var $l = query.insertId;
                        if (!err) {
                            var tb = App.detalle.mainTabla.find('tbody');
                            var r = [];
                            tb.find('tr:not(.nodata)').each(function (i, e) {
                                r.push($(this).attr('data-pk'));
                            });
                            if (r.length > 0) {
                                App.detalle.dataContainer.val(r);
                                console.log(q);
                                App.core.loadMysql(function () {
                                    var q = 'DELETE FROM fbr_despacho_detalle WHERE iddespacho = ' + $l + ';';
                                    App.core.connection.query(q, function (err, query, fields) {
                                        if (!err) {
                                            App.core.loadMysql(function () {
                                                var q = 'INSERT INTO fbr_despacho_detalle (iddespacho,idrecepcion_pallet)\n\
                                                        VALUES';

                                                for (var i = 0; i < r.length; i++) {
                                                    q += '(' + $l + ',' + r[i] + '),';
                                                }
                                                q = q.substring(0, q.length - 1);
                                                App.core.connection.query(q, function (err, query, fields) {
                                                    if (!err) {
                                                        blackout.hide(function () {
                                                            App.clean();
                                                            $.gritter.add({
                                                                title: "OK!",
                                                                text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                                                class_name: 'growl-success',
                                                                sticky: false,
                                                                time: 2000
                                                            });
                                                        });
                                                    } else {
                                                        console.log(err);
                                                        blackout.hide(function () {
                                                            $.gritter.add({
                                                                title: "Error!",
                                                                text: "<i class='fa fa-clock-o'></i> <i>Error al guardar los datos</i>",
                                                                class_name: 'growl-danger',
                                                                sticky: false,
                                                                time: 2000
                                                            });
                                                        });
                                                    }
                                                    ;
                                                });
                                            });
                                        } else {
                                            console.log(err);
                                            blackout.hide(function () {
                                                $.gritter.add({
                                                    title: "Error!",
                                                    text: "<i class='fa fa-clock-o'></i> <i>Error al guardar los datos</i>",
                                                    class_name: 'growl-danger',
                                                    sticky: false,
                                                    time: 2000
                                                });
                                            });
                                        }
                                    });
                                });

                            } else {
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Este Despacho no posee pallets, debe al menos ingresar un pallet</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 4000
                                    });
                                });
                            }
                        } else {
                            console.log(err);
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "Error",
                                    text: "<i class='fa fa-clock-o'></i> <i>Error al duardar los datos</i>",
                                    class_name: 'growl-danger',
                                    sticky: false,
                                    time: 4000
                                });
                            });
                        }
                    });
                });
            });
        } else {
            App.core.loadMysql(function () {
                var q = 'UPDATE fbr_despacho \n\
                            SET\n\
                            fecha_operacion = "' + $('[name=fecha_operacion]').val() + '",\n\
                            hora_despacho = "' + $('[name=hora_despacho]').val() + '",\n\
                            num_guia = "' + $('[name=num_guia]').val() + '",\n\
                            patente = "' + $('[name=patente]').val() + '",\n\
                            nombre_conductor = "' + $('[name=nombre_conductor]').val() + '",\n\
                            sello_1 = "' + $('[name=sello_1]').val() + '",\n\
                            sello_2 = "' + $('[name=sello_2]').val() + '",\n\
                            fecha_edicion = NOW(),\n\
                            idmodificador = ' + App.GLOBAL.USUARIO + ' WHERE iddespacho = ' + idpk + ';';
                App.core.connection.query(q, function (err, query, fields) {
                    if (!err) {
                        var tb = App.detalle.mainTabla.find('tbody');
                        var r = [];
                        tb.find('tr:not(.nodata)').each(function (i, e) {
                            r.push($(this).attr('data-pk'));
                        });
                        if (r.length > 0) {
                            App.detalle.dataContainer.val(r);
                            console.log(q);
                            App.core.loadMysql(function () {
                                var q = 'DELETE FROM fbr_despacho_detalle WHERE iddespacho = ' + $(".pk_form").val() + ';';
                                App.core.connection.query(q, function (err, query, fields) {
                                    if (!err) {
                                        App.core.loadMysql(function () {
                                            var q = 'INSERT INTO fbr_despacho_detalle (iddespacho,idrecepcion_pallet)\n\
                                                        VALUES';

                                            for (var i = 0; i < r.length; i++) {
                                                q += '(' + $(".pk_form").val() + ',' + r[i] + '),';
                                            }
                                            q = q.substring(0, q.length - 1);
                                            App.core.connection.query(q, function (err, query, fields) {
                                                if (!err) {
                                                    blackout.hide(function () {
                                                        App.clean();
                                                        $.gritter.add({
                                                            title: "OK!",
                                                            text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                                            class_name: 'growl-success',
                                                            sticky: false,
                                                            time: 2000
                                                        });
                                                    });
                                                } else {
                                                    console.log(err);
                                                    blackout.hide(function () {
                                                        $.gritter.add({
                                                            title: "Error!",
                                                            text: "<i class='fa fa-clock-o'></i> <i>Error al guardar los datos</i>",
                                                            class_name: 'growl-danger',
                                                            sticky: false,
                                                            time: 2000
                                                        });
                                                    });
                                                }
                                                ;
                                            });
                                        });
                                    } else {
                                        console.log(err);
                                        blackout.hide(function () {
                                            $.gritter.add({
                                                title: "Error!",
                                                text: "<i class='fa fa-clock-o'></i> <i>Error al guardar los datos</i>",
                                                class_name: 'growl-danger',
                                                sticky: false,
                                                time: 2000
                                            });
                                        });
                                    }
                                });
                            });

                        } else {
                            console.log(r.length);
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "Error",
                                    text: "<i class='fa fa-clock-o'></i> <i>Este Despacho no posee pallets, debe al menos ingresar un pallet</i>",
                                    class_name: 'growl-danger',
                                    sticky: false,
                                    time: 4000
                                });
                            });
                        }
                    }
                });
            });
        }
    }
});

App.clean = function () {
    $(".pk_form").val(0);
    $('[name=fecha_operacion]').val('');
    $('[name=hora_despacho]').val('');
    $('[name=nombre_conductor]').val('');
    $('[name=num_guia]').val('');
    $('[name=patente]').val('');
    $('[name=sello_1]').val('');
    $('[name=sello_2]').val('');
    App.detalle.mainTabla.find('tbody').html('');
    $('[data-maincontainerform]').slideUp();
};

//App.core.loadMysql(function () {
//App.core.connection.query(q, function (err, query, fields) {

App.editar = function (idpk) {
    blackout.show('Cargando...', function () {
        App.core.loadMysql(function () {
            var q = 'SELECT * FROM fbr_despacho WHERE iddespacho = ' + idpk + ';';
            App.core.connection.query(q, function (err, rows, fields) {
                if (!err) {
                    var $data = rows[0];
                    App.core.loadMysql(function () {
                        var q = 'SELECT * FROM fbr_v_despachos_detalle WHERE iddespacho = ' + idpk + ';';
                        App.core.connection.query(q, function (err, rows, fields) {
                            if (!err) {
                                blackout.hide(function () {
                                    $('[data-maincontainerform]').slideDown();
                                    var $table = rows;
                                    $(".pk_form").val(idpk);
                                    $('[name=fecha_operacion]').val($data.fecha_operacion);
                                    $('[name=hora_despacho]').val($data.hora_despacho);
                                    $('[name=nombre_conductor]').val($data.nombre_conductor);
                                    $('[name=num_guia]').val($data.num_guia);
                                    $('[name=patente]').val($data.patente);
                                    $('[name=sello_1]').val($data.sello_1);
                                    $('[name=sello_2]').val($data.sello_2);
                                    App.detalle.drawTable($table);
                                    App.detalle.addUsedData($table);
                                });
                            } else {
                                console.log(err);
                                blackout.hide(function () {
                                    $.gritter.add({
                                        title: "Error",
                                        text: "<i class='fa fa-clock-o'></i> <i>Error cargando los datos</i>",
                                        class_name: 'growl-danger',
                                        sticky: false,
                                        time: 4000
                                    });
                                });
                            }
                        });
                    });
                } else {
                    blackout.hide(function () {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>Error cargando los datos</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 4000
                        });
                    });
                }
            });
        });
    });
};

App.eliminar = function (idpk) {
    App.core.loadMysql(function () {
        var q = 'SELECT * FROM fbr_despacho WHERE iddespacho = ' + idpk + ' LIMIT 1;';
        App.core.connection.query(q, function (err, rows, fields) {
            if (!err) {
                var iddespacho = rows[0];
                App.core.loadMysql(function () {
                    var q = 'SELECT * FROM fbr_despacho_detalle WHERE iddespacho = ' + iddespacho + ';';
                    App.core.connection.query(q, function (err, all, fields) {
                        if (!err) {
                            var $all = all;
                        } else {
                            console.log(err);
                        }
                    });
                });
            } else {
                console.log(err);
            }
        });
    });
};

App.comprobante = function (idpk) {
    blackout.show('Generando...', function () {
        var html = '<style>\n\
    *{\n\
        font-family: Helvetica;\n\
    }\n\
    .maintable,\n\
    .maintable>tr,\n\
    .maintable>tr>td  {\n\
        border: 0;\n\
        border-collapse: collapse;\n\
        font-size:10px;\n\
    }\n\
    table table, table table th, table table td {\n\
        border: 1px solid black;\n\
        border-collapse: collapse;\n\
        font-size:10px;\n\
    }\n\
    table table.noborder, table table.noborder th, table table.noborder td {\n\
        border: 0px solid black;\n\
        border-collapse: collapse;\n\
        font-size:10px;\n\
    }\n\
    .small{\n\
        font-size:8px;\n\
    }\n\
</style>';
        App.core.loadMysql(function () {
            var q = 'SELECT * FROM fbr_v_despachos WHERE iddespacho = ' + idpk + ' LIMIT 1;';
            App.core.connection.query(q, function (err, rows, fields) {
                if (!err) {

                    var header = rows[0];
                    var $logo = '<img src="http://baladm.cl/framberry/wp-content/uploads/2016/05/logo_framberry.png" width="70" height="50">';
                    html += '<div>';
                    html += '<table style="position:absolute;left:20px;top:20px; font-size:12px;" width="570" class="maintable">';
                    html += '<tr>\n\
                            <td colspan=2>\n\
                                <table style="width:100%;">\n\
                                    <tr>\n\
                                        <th rowspan=2>\n\
                                            <p style="font-size:8px;font-weight:normal;" class="small">' + $logo + '</p></th>\n\
                                        <th>COMPROBANTE DE DESPACHO</th>\n\
                                        <th align="left"><p class="small">\n\
                                                Fecha : ' + header.fecha_operacion + '<br>\n\
                                                Hora Operacion : ' + header.hora_despacho + '</p>\n\
                                        </th>\n\
                                    </tr>\n\
                                </table>\n\
                            </td>\n\
                        </tr>';
                    html += '<tr>\n\
                            <td colspan=2 style="padding: 6px 0px 8px 0px;">\n\
                                <table style="width:100%;" class="noborder">\n\
                                    <tr>\n\
                                        <td><p class=""><b>Acopio:</b> ' + header.acopio + ' </p></td>\n\
                                        <td><p class=""><b>N° de Guia</b> ' + header.num_guia + ' </p></td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><p class=""><b>Patente:</b> ' + header.patente + ' </p></td>\n\
                                        <td><p class=""><b>Conductor:</b> ' + header.nombre_conductor + ' </p></td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td><p class=""><b>1er Sello Camión:</b> ' + header.sello_1 + ' </p></td>\n\
                                        <td><p class=""><b>2do Sello Camión:</b> ' + header.sello_2 + '</p></td>\n\
                                    </tr>\n\
                                </table>\n\
                            </td>\n\
                        </tr>';
                    html += '<tr>\n\
                            <td align="center">\n\
                                <table style="width:80%;">\n\
                                    <tr>\n\
                                        <th colspan="5">Pallets</th>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <th>Folio</th>\n\
                                        <th>Producto</th>\n\
                                        <th>Envase</th>\n\
                                        <th>P. Neto</th>\n\
                                        <th>p. Bruto</th>\n\
                                    </tr>';

                    App.core.loadMysql(function () {
//                                                var q = 'SELECT * FROM fbr_v_despacho_detalles_full WHERE iddespacho = ' + idpk + ';';
                        var q = 'SELECT \n\
                            `fbr_despacho_detalle`.`iddespacho` AS `iddespacho`,\n\
                            `fbr_despacho_detalle`.`idrecepcion_pallet` AS `idrecepcion_pallet`,\n\
                            `fbr_recepcion_pallet`.`folio` AS `folio`,\n\
                            `fbr_recepcion_pallet`.`idproducto` AS `idproducto`,\n\
                            `fbr_producto`.`codproducto` AS `codproducto`,\n\
                            `fbr_producto`.`descripcion` AS `descripcion`,\n\
                            `fbr_recepcion_pallet`.`correlativo` AS `correlativo`,\n\
                            `fbr_recepcion_pallet`.`estado` AS `estado`,\n\
                            `fbr_recepcion_pallet`.`despachado` AS `despachado`,\n\
                            `fbr_recepcion_pallet`.`fecha_operacion` AS `fecha_operacion`,\n\
                            `fbr_recepcion_pallet_pesaje`.`idpallet` AS `idpallet`,\n\
                            `BP`.`peso` AS `peso_base_pallet`,\n\
                            round(SUM(`fbr_recepcion_pesaje`.`cant_bandejas`),2) AS `all_bandejas`,\n\
                            round(SUM(`fbr_recepcion_pesaje`.`kilos_netos`),2) AS `all_kilos_neto`,\n\
                            round((`fbr_envase`.`peso` * SUM(`fbr_recepcion_pesaje`.`cant_bandejas`)),2) AS `peso_envase_all`\n\
                        FROM\n\
                            `fbr_despacho_detalle`\n\
                            JOIN `fbr_recepcion_pallet` ON `fbr_despacho_detalle`.`idrecepcion_pallet` = `fbr_recepcion_pallet`.`idpallet`\n\
                            JOIN `fbr_producto` ON `fbr_producto`.`idproducto` = `fbr_recepcion_pallet`.`idproducto`\n\
                            left join fbr_recepcion_pallet_pesaje on fbr_recepcion_pallet_pesaje.idpallet = fbr_despacho_detalle.idrecepcion_pallet\n\
                            JOIN `fbr_recepcion_pesaje` ON `fbr_recepcion_pesaje`.`idpesaje` = `fbr_recepcion_pallet_pesaje`.`idpesaje`\n\
                            JOIN `fbr_envase` `BP` ON `BP`.`idenvase` = `fbr_producto`.`idbase_pallet`\n\
                            JOIN `fbr_envase` ON `fbr_envase`.`idenvase` = `fbr_recepcion_pesaje`.`idenvase`\n\
                        where `fbr_despacho_detalle`.`iddespacho` = ' + idpk + '\n\
                        GROUP BY `fbr_recepcion_pallet_pesaje`.`idpallet` , `fbr_recepcion_pesaje`.`idenvase`';
                        App.core.connection.query(q, function (err, rows, fields) {
                            if (!err) {
                                var tEnvase = 0;
                                var tNeto = 0;
                                var tBruto = 0;
                                var peso_bruto = 0;
                                for (var i = 0; i < rows.length; i++) {
                                    var det = rows[i];
                                    html += '<tr>\n\
                                                <td>' + det.folio + '</td>\n\
                                                <td>' + det.codproducto + ' - ' + det.descripcion + '</td>\n\
                                                <td align="right">' + det.all_bandejas + '</td>\n\
                                                <td align="right">' + det.all_kilos_neto + '</td>\n\
                                                <td align="right">' + (det.peso_envase_all + det.peso_base_pallet + det.all_kilos_neto) + '</td>';
                                    html += '</tr>';
                                    tEnvase += det.all_bandejas;
                                    tNeto += det.all_kilos_neto;
                                    tBruto += det.peso_envase_all + det.peso_base_pallet + det.all_kilos_neto;
                                }
                                html += '<tr>\n\
                                                                    <th colspan="2">Totales</th>\n\
                                                                    <th>' + tEnvase + '</th>\n\
                                                                    <th>' + tNeto + '</th>\n\
                                                                    <th>' + tBruto + '</th>\n\
                                                                </tr>';
                                html += '</table>\n\
                                                        </td>\n\
                                                    </tr>';
                                html += '</table>';
                                html += '</div>';
                                blackout.hide(function () {
//                                    App.core.pdf.create(html, {"format": "Letter"}).toStream(function (err, stream) {
//                                        if (!err) {
//                                            stream.pipe(App.core.fs.createWriteStream('./Comprobante_despacho.pdf'));
////                                                                        setTimeout(function () {
////                                                                            App.electr = require('electron');
////                                                                            var win = new App.electr.BrowserWindow({width: 800, height: 600});
////                                                                            win.loadURL("file://" + __dirname + "/Comprobante.pdf");
////                                                                        }, 1);
//                                        } else {
//                                            blackout.hide();
//                                            console.log(err + '11');
//                                        }
//                                    });
                                            App.core.pdf.create(html, {"format": "Letter"}).toStream(function (err, stream) {
                                                if (!err) {
                                                const {dialog} = require('electron').remote;
                                                blackout.hide(function () {
                                                    dialog.showSaveDialog({
                                                        title: "Guardar Reporte",
                                                        defaultPath: "Comprobante.pdf"
                                                    },
                                                            function (fileName) {
                                                                stream.pipe(App.core.fs.createWriteStream(fileName));
                                                            });
                                                });
                                                } else {
                                                    console.log(err);
                                                    blackout.hide();
                                                }
                                            });
                                });
                            } else {
                                console.log(err + '22');
                                blackout.hide();
                            }
                        });
                    });

                } else {
                    console.log(err + '44');
                    blackout.hide();
                }
            });
        });
    });
};
App.cerrar = function (idpk) {
    App.xDom.closeForm(function () {
        xmodal.show(null, "¿Está seguro que desea cerrar este despacho?", function () {
            $.ajax({
                type: "POST",
                url: "[:__BASE_URL:]index.acopio/despacho/close",
                data: "id=" + idpk
            }).done(function (data) {
                try {
                    var r = JSON.parse(data);
                    if (r.response === JSONTRUE) {
                        App.oTable.fnDraw();
                        $.gritter.add({
                            title: "OK!",
                            text: "<i class='fa fa-clock-o'></i> <i>El despacho se ha cerrado con éxito</i>",
                            class_name: 'growl-success',
                            sticky: false,
                            time: 2000
                        });
                        xmodal.hide();
                    } else {
                        $.gritter.add({
                            title: "Error",
                            text: "<i class='fa fa-clock-o'></i> <i>No hemos podido cerrar el despacho</i>",
                            class_name: 'growl-danger',
                            sticky: false,
                            time: 2000
                        });
                    }
                    xmodal.hide();
                } catch (e) {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>No hemos podido cerrar el despacho</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 2000
                    });
                    xmodal.hide();
                }
            }).error(function () {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>No hemos podido cerrar el despacho</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 2000
                });
                xmodal.hide();
            });
        });
    });
};
App.guia = function (idpk) {
    var modal = $("#PrintGuiaModal");
    var cont = modal.find('.printer_modal');
    cont.find('[name="iddespacho_selected"]').val(idpk);
    cont.find('[name="idconf_impresion"]').val(null).trigger('chosen:updated');
    modal.modal('show');
    $("[data-continueprinter]").bind('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var fm = cont.find('[name="idconf_impresion"]').val();
        var pk = cont.find('[name="iddespacho_selected"]').val();
        if (fm) {
            window.open('[:__BASE_URL:]index.acopio/despacho/get_guia/' + pk + '/' + btoa(fm));
        } else {
            $.gritter.add({
                title: "Error",
                text: "<i class='fa fa-clock-o'></i> <i>Debe seleccionar un formato de impresión para continuar</i>",
                class_name: 'growl-danger',
                sticky: false,
                time: 4000
            });
        }
    });
};
App.palletDisponibles = function (callbacki) {
    App.core.loadMysql(function () {
        var q = 'SELECT * FROM fbr_v_pallets WHERE activo = 1 AND idacopio = ' + App.GLOBAL.ACOPIO + ' AND idtemporada = ' + App.GLOBAL.TEMPORADA + ' AND despachado = 0 AND estado = 2;';
        App.core.connection.query(q, function (err, rows, fields) {
            if (!err) {
                var $r = [];
                for (var i = 0; i < rows.length; i++) {
                    var o = {};
                    o.id = rows[i].idpallet;
                    o.folio = rows[i].folio_i;
                    o.label = rows[i].folio_i + ' - ' + rows[i].producto;
                    $r.push(o);
                }
                callbacki($r);
            } else {
                console.log(q);
                console.log(err);
            }
        });
    });
};
var singleton = function () {
    App.detalle.init();
};
var Pallets = function () {
    this.mainTabla = $("#despachoDetalle");
    this.mainForm = $("#second-form");
    this.dataContainer = $("#despachodata");
    this.mainOptionPallets = $('#second-form [name="idpallet"]');
    this.optionsPallets = null;
    App.palletDisponibles(function (rr) {
        App.detalle.optionsPallets = rr;
        singleton();
    });
    this.usedPallets = [];
    this.errorPallet = "Este folio no corresponde a un pallet.";
};
Pallets.prototype.serialize = function () {
    var tb = this.mainTabla.find('tbody');
    var r = [];
    tb.find('tr:not(.nodata)').each(function (i, e) {
        r.push($(this).attr('data-pk'));
    });
    if (r.length > 0) {
        this.dataContainer.val(JSON.stringify(r));
        return true;
    } else {
        return false;
    }
};
Pallets.prototype.unsetOption = function (id) {
    for (var i = 0; i < this.optionsPallets.length; i++) {
        var a = this.optionsPallets[i];
        if (parseInt(a.id) === id) {
            this.usedPallets.push(a);
            this.optionsPallets.splice(i, 1);
            break;
        }
    }
};
Pallets.prototype.resetOption = function (id) {
    for (var i = 0; i < this.usedPallets.length; i++) {
        var a = this.usedPallets[i];
        if (parseInt(a.id) === id) {
            this.optionsPallets.push(a);
            this.usedPallets.splice(i, 1);
            break;
        }
    }
};
Pallets.prototype.init = function () {
    $.validator.addMethod("palletExists", function (value, element) {
        App.errtype = 0;
        for (var i = 0; i < App.detalle.optionsPallets.length; i++) {
            var a = App.detalle.optionsPallets[i];
            if (parseInt(a.folio) === parseInt(value)) {
                return true;
            }
        }
        for (var i = 0; i < App.detalle.usedPallets.length; i++) {
            var a = App.detalle.usedPallets[i];
            if (parseInt(a.folio) === parseInt(value)) {
                App.errtype = 1;
                return false;
            }
        }
        App.errtype = 2;
        return false;
    }, function () {
        switch (App.errtype) {
            case 0:
                return "";
            case 1:
                return "El pallet correspondiente a este folio ya ha sido asignado";
            case 2:
                return "Este folio no corresponde a un pallet.";
        }
    });
    $('[name="patente"]').mask('AA-AA-99', {
        translation: {
            placeholder: "  -  -  "
        }
    });
    $("#second-form").validate({
        rules: {
            idpallet: {
                required: true,
                palletExists: true
            }
        },
        messages: {
            idpallet: {
                required: "Debe ingresar el folio de un pallet"
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            element.after(error);
        },
        submitHandler: function (form) {
            var idpallet = App.detalle.getIdPallet($(form).find('[name="idpallet"]').val());
            var r = '<tr data-pk="' + idpallet + '">\n\
                        <td>' + App.detalle.findName(parseInt(idpallet)) + '</td>\n\
                        <td><button class="btn btn-danger btn-xs" onclick="App.detalle.deletePallet(\'' + idpallet + '\');return false;" type="button">Eliminar</button></td>\n\
                    </tr>';
            var tb = App.detalle.mainTabla.find('tbody');
            if (tb.find('tr:not(.nodata)').length !== 0) {
                tb.append(r);
            } else {
                tb.append(r);
                tb.find('tr.nodata').hide();
            }
//            App.xDom.refreshForm("#second-form");
            App.detalle.unsetOption(parseInt(idpallet));
        }
    });
};
Pallets.prototype.drawTable = function (data) {
    var tb = App.detalle.mainTabla.find('tbody');
    var r = '';
    for (var i = 0; i < data.length; i++) {
        var a = data[i];
        r += '<tr data-pk="' + a.idrecepcion_pallet + '">\n\
                    <td>' + a.label + '</td>\n\
                    <td><button class="btn btn-danger btn-xs" onclick="App.detalle.deletePallet(\'' + a.idrecepcion_pallet + '\');return false;" type="button">Eliminar</button></td>\n\
                </tr>';
    }
    tb.find('tr.nodata').hide();
    tb.append(r);
    if (tb.find('tr:not(.nodata)').length === 0) {
        tb.find('tr.nodata').slideDown();
    }
};
Pallets.prototype.addUsedData = function (data) {
    for (var i = 0; i < data.length; i++) {
        var a = data[i];
        var o = {
            id: parseInt(a.idrecepcion_pallet),
            label: a.label
        };
        App.detalle.usedPallets.push(o);
    }
};
Pallets.prototype.findName = function (id) {
    for (var i = 0; i < this.optionsPallets.length; i++) {
        var a = this.optionsPallets[i];
        if (parseInt(a.id) === id) {
            return a.label;
        }
    }
};
Pallets.prototype.getIdPallet = function (id) {
    for (var i = 0; i < this.optionsPallets.length; i++) {
        var a = this.optionsPallets[i];
        if (parseInt(a.folio) === parseInt(id)) {
            return a.id;
        }
    }
};
Pallets.prototype.deletePallet = function (id) {
    var tb = this.mainTabla.find('tbody');
    tb.find('tr[data-pk="' + id + '"]').remove();
    if (tb.find('tr:not(.nodata)').length === 0) {
        tb.find('tr.nodata').slideDown();
    }
    App.detalle.resetOption(parseInt(id));
};
Pallets.prototype.cleanTable = function () {
    var tb = this.mainTabla.find('tbody');
    tb.find('tr:not(.nodata)').remove();
    tb.find('tr.nodata').show();
};
App.detalle = new Pallets();