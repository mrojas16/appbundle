/* global App, blackout */

$("#fromserver-form").validate({
    rules: {
    },
    messages: {
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (f) {
        blackout.show("Espere...", function () {
            var form = $(f);
            var chkd = form.find(':checked');
            if (chkd.length > 0) {
                var data = {};
                data.identification = {};

                data.local = {
                    ACOPIO: App.GLOBAL.ACOPIO,
                    TEMPORADA: App.GLOBAL.TEMPORADA,
                    USUARIO: App.GLOBAL.USUARIO
                };
                data.request = [];

                App.core.storage.get('apiconnection', function (error, d) {
                    if (error) {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos de la API</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 4000
                            });
                        });
                    } else {
                        var server = d.api_serv;
                        data.identification.user = d.api_user;
                        data.identification.key = d.api_pass;
                        chkd.each(function () {
                            data.request.push($(this).val());
                        });
                        var hh = data.request;
                        var z = btoa(JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            url: server + "get/" + z
                        }).done(function (data) {
                            setTimeout(function () {
                                try {
                                    var r = JSON.parse(data);
                                    if (r.response === 1) {

                                        var handler = new Put();
                                        handler.then(function () {
                                            blackout.hide(function () {
                                                $.gritter.add({
                                                    title: "OK!",
                                                    text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                                    class_name: 'growl-success',
                                                    sticky: false,
                                                    time: 2000
                                                });
                                            });
                                        }).error(function (e) {
                                            console.log(e);
                                            blackout.hide(function () {
                                                $.gritter.add({
                                                    title: "Error",
                                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                                                    class_name: 'growl-danger',
                                                    sticky: false,
                                                    time: 2000
                                                });
                                            });
                                        });

                                        handler.complete(r.data, hh);
                                    } else {
                                        blackout.hide(function () {
                                            $.gritter.add({
                                                title: "Error",
                                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                                                class_name: 'growl-danger',
                                                sticky: false,
                                                time: 2000
                                            });
                                        });
                                    }
                                } catch (e) {
                                    blackout.hide(function () {
                                        $.gritter.add({
                                            title: "Error",
                                            text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                                            class_name: 'growl-danger',
                                            sticky: false,
                                            time: 2000
                                        });
                                    });
                                }
                            }, 1);
                        });
                    }
                });

            } else {
                blackout.hide(function () {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>Debe seleccionar al menos un elemento a sincronizar</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 4000
                    });
                });
            }
        });
    }
});

$("#toserver-form").validate({
    rules: {
    },
    messages: {
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (f) {
        blackout.show("Espere...", function () {
            var form = $(f);
            var chkd = form.find(':checked');
            if (chkd.length > 0) {
                var data = {};
                data.identification = {};

                data.local = {
                    ACOPIO: App.GLOBAL.ACOPIO,
                    TEMPORADA: App.GLOBAL.TEMPORADA,
                    USUARIO: App.GLOBAL.USUARIO
                };
                data.data = [];

                App.core.storage.get('apiconnection', function (error, d) {
                    if (error) {
                        blackout.hide(function () {
                            $.gritter.add({
                                title: "Error",
                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos de la API</i>",
                                class_name: 'growl-danger',
                                sticky: false,
                                time: 4000
                            });
                        });
                    } else {
                        var server = d.api_serv;
                        data.identification.user = d.api_user;
                        data.identification.key = d.api_pass;
                        chkd.each(function () {
                            data.data.push($(this).val());
                        });
                        var hh = data.data;
                        var handler = new Get();
                        handler.then(function (z) {
                            console.log(z);
                            $.ajax({
                                type: "POST",
                                url: server + "put",
                                data: "data=" + z
                            }).done(function (data) {
                                console.log(data);
                                setTimeout(function () {
                                    try {
                                        var r = JSON.parse(data);
                                        if (r.response === 1) {
                                            blackout.hide(function () {
                                                $.gritter.add({
                                                    title: "OK!",
                                                    text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                                                    class_name: 'growl-success',
                                                    sticky: false,
                                                    time: 2000
                                                });
                                            });
                                        } else {
                                            blackout.hide(function () {
                                                $.gritter.add({
                                                    title: "Error",
                                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                                                    class_name: 'growl-danger',
                                                    sticky: false,
                                                    time: 2000
                                                });
                                            });
                                        }
                                    } catch (e) {
                                        blackout.hide(function () {
                                            $.gritter.add({
                                                title: "Error",
                                                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                                                class_name: 'growl-danger',
                                                sticky: false,
                                                time: 2000
                                            });
                                        });
                                    }
                                }, 1);
                            });
                        }).error(function () {
                            blackout.hide(function () {
                                $.gritter.add({
                                    title: "Error",
                                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos</i>",
                                    class_name: 'growl-danger',
                                    sticky: false,
                                    time: 2000
                                });
                            });
                        });

                        handler.complete(data, hh);
                    }
                });

            } else {
                blackout.hide(function () {
                    $.gritter.add({
                        title: "Error",
                        text: "<i class='fa fa-clock-o'></i> <i>Debe seleccionar al menos un elemento a sincronizar</i>",
                        class_name: 'growl-danger',
                        sticky: false,
                        time: 4000
                    });
                });
            }
        });
    }
});

function Put() {

    var self = this;
    var thenCallback = null;
    var failCallback = null;

    self.then = function (callback) {
        thenCallback = callback;
        return self;
    };

    self.error = function (callback) {
        failCallback = callback;
        return self;
    };

    self.complete = function (data, requestdata) {
        var counter = 0, pc = 0;
        for (var i = 0; i < requestdata.length; i++) {
            switch (parseInt(requestdata[i])) {
                case 1:
                    counter += data.productores.length;
                    break;
                case 2:
                    counter += data.envases.length;
                    break;
                case 3:
                    counter += data.fbr_acopio.length;
                    break;
                case 4:
                    counter += data.fbr_bodega.length;
                    break;
                case 5:
                    counter += data.fbr_especie.length;
                    break;
                case 6:
                    counter += data.fbr_especie_acopio.length;
                    break;
                case 7:
                    counter += data.fbr_parametro.length;
                    break;
                case 8:
                    counter += data.fbr_param_especie.length;
                    break;
                case 9:
                    counter += data.fbr_precio_semanal.length;
                    break;
                case 10:
                    counter += data.fbr_producto.length;
                    break;
                case 11:
                    counter += data.fbr_variedad.length;
                    break;
            }
        }

        var names = {
            "1": "productores",
            "2": "envases",
            "3": "fbr_acopio",
            "4": "fbr_bodega",
            "5": "fbr_especie",
            "6": "fbr_especie_acopio",
            "7": "fbr_parametro",
            "8": "fbr_param_especie",
            "9": "fbr_precio_semanal",
            "10": "fbr_producto",
            "11": "fbr_variedad"
        };

        App.core.loadMysql(function () {
            for (var i = 0; i < requestdata.length; i++) {
                switch (parseInt(requestdata[i])) {
                    case 1:
                        var q0 = "DELETE FROM fbr_productor WHERE idproductor >0";
                        App.core.connection.query(q0, function (err, open, fields) {
                            if (!err) {
                                for (var j = 0; j < data.productores.length; j++) {
                                    var prod = data.productores[j];
                                    var q = "INSERT IGNORE INTO `fbr_productor` ( \n\
                                        `idproductor`, \n\
                                        `nombre_fantasia`, \n\
                                        `razon_social`, \n\
                                        `rut`, \n\
                                        `representante_legal`, \n\
                                        `giro`, \n\
                                        `idcomuna`, \n\
                                        `direccion`, \n\
                                        `nombre_personal`, \n\
                                        `apaterno_personal`, \n\
                                        `amaterno_personal`, \n\
                                        `tipo_pago`, \n\
                                        `cuenta`, \n\
                                        `mails`, \n\
                                        `fecha_creacion`, \n\
                                        `fecha_edicion`, \n\
                                        `fecha_eliminacion`, \n\
                                        `idcreador`, \n\
                                        `idmodificador`, \n\
                                        `ideliminador`, \n\
                                        `activo`\n\
                                    ) VALUES ( \n\
                                        '" + prod.idproductor + "', \n\
                                        '" + prod.nombre_fantasia + "', \n\
                                        '" + prod.razon_social + "', \n\
                                        '" + prod.rut + "', \n\
                                        '" + prod.representante_legal + "', \n\
                                        '" + prod.giro + "', \n\
                                        '" + prod.idcomuna + "', \n\
                                        '" + prod.direccion + "', \n\
                                        '" + prod.nombre_personal + "', \n\
                                        '" + prod.apaterno_personal + "', \n\
                                        '" + prod.amaterno_personal + "', \n\
                                        '" + prod.tipo_pago + "', \n\
                                        '" + prod.cuenta + "', \n\
                                        '" + prod.mails + "', \n\
                                        '" + prod.fecha_creacion + "', \n\
                                        '" + prod.fecha_edicion + "', \n\
                                        '" + prod.fecha_eliminacion + "', \n\
                                        '" + prod.idcreador + "', \n\
                                        '" + prod.idmodificador + "', \n\
                                        '" + prod.ideliminador + "', \n\
                                        '" + prod.activo + "'\n\
                                    );";

                                    App.core.connection.query(q, function (err, open, fields) {
                                        pc++;
                                        if (!err) {
                                            if (pc === counter) {
                                                return runCallback(thenCallback);
                                            }
                                        } else {
                                            return runCallback(failCallback, err);
                                        }
                                    });
                                }
                            } else {
                                return runCallback(failCallback, err);
                            }
                        });
                        break;
                    case 2:
                        var q0 = "DELETE FROM fbr_envase WHERE idenvase > 0";
                        App.core.connection.query(q0, function (err, open, fields) {
                            if (!err) {
                                for (var j = 0; j < data.envases.length; j++) {
                                    var env = data.envases[j];
                                    var q = "INSERT IGNORE INTO fbr_envase(\n\
                                        `idenvase`, \n\
                                        `codenvase`, \n\
                                        `tipo`, \n\
                                        `descripcion`, \n\
                                        `peso`, \n\
                                        `peso_minimo`, \n\
                                        `peso_maximo`, \n\
                                        `fecha_creacion`, \n\
                                        `fecha_edicion`, \n\
                                        `fecha_eliminacion`, \n\
                                        `idcreador`, \n\
                                        `idmodificador`, \n\
                                        `ideliminador`, \n\
                                        `activo`\n\
                                    ) VALUES ( \n\
                                        '" + env.idenvase + "', \n\
                                        '" + env.codenvase + "', \n\
                                        '" + env.tipo + "', \n\
                                        '" + env.descripcion + "', \n\
                                        '" + env.peso + "', \n\
                                        '" + env.peso_minimo + "', \n\
                                        '" + env.peso_maximo + "', \n\
                                        '" + env.fecha_creacion + "', \n\
                                        '" + env.fecha_edicion + "', \n\
                                        '" + env.fecha_eliminacion + "', \n\
                                        '" + env.idcreador + "', \n\
                                        '" + env.idmodificador + "', \n\
                                        '" + env.ideliminador + "', \n\
                                        '" + env.activo + "' \n\
                                    );";

                                    App.core.connection.query(q, function (err, open, fields) {
                                        pc++;
                                        if (!err) {
                                            if (pc === counter) {
                                                return runCallback(thenCallback);
                                            }
                                        } else {
                                            return runCallback(failCallback, err);
                                        }
                                    });
                                }
                            } else {
                                return runCallback(failCallback, err);
                            }
                        });
                        break;
                    case 3:
                        var q0 = "DELETE FROM fbr_acopio WHERE idacopio > 0";
                        App.core.connection.query(q0, function (err, open, fields) {
                            if (!err) {
                                console.log([pc, counter]);
                                for (var j = 0; j < data.fbr_acopio.length; j++) {
                                    var dt = data.fbr_acopio[j];
                                    console.log(dt);
                                    var q = "INSERT INTO `fbr_acopio` (\n\
                                    `idacopio`,\n\
                                    `codacopio`,\n\
                                    `descripcion`,\n\
                                    `coderp`,\n\
                                    `tipo_pesaje`,\n\
                                    `is_acopioplanta`,\n\
                                    `activo`\n\
                                ) VALUES (\n\
                                    '" + dt.idacopio + "',\n\
                                    '" + dt.codacopio + "',\n\
                                    '" + dt.descripcion + "',\n\
                                    '" + dt.coderp + "',\n\
                                    '" + dt.tipo_pesaje + "',\n\
                                    '" + dt.is_acopioplanta + "',\n\
                                    '" + dt.activo + "'\n\
                                );";

                                    App.core.connection.query(q, function (err, open, fields) {
                                        pc++;
                                        if (!err) {
                                            if (pc === counter) {
                                                return runCallback(thenCallback);
                                            }
                                        } else {
                                            console.log(err);
                                            return runCallback(failCallback, err);
                                        }
                                    });
                                }
                            } else {
                                console.log(err);
                                return runCallback(failCallback, err);
                            }
                        });
                        break;
                    case 4:
                        var q0 = "DELETE FROM fbr_bodega WHERE idbodega> 0";
                        App.core.connection.query(q0, function (err, open, fields) {
                            if (!err) {
                                for (var j = 0; j < data.fbr_bodega.length; j++) {
                                    var dt = data.fbr_bodega[j];
                                    var q = "INSERT INTO `fbr_bodega` (\n\
                                    `idbodega`,\n\
                                    `descripcion`,\n\
                                    `codbodega`,\n\
                                    `idacopio`,\n\
                                    `fecha_creacion`,\n\
                                    `idcreador`,\n\
                                    `activo`\n\
                                ) VALUES (\n\
                                    '" + dt.idbodega + "',\n\
                                    '" + dt.descripcion + "',\n\
                                    '" + dt.codbodega + "',\n\
                                    '" + dt.idacopio + "',\n\
                                    '" + dt.fecha_creacion + "',\n\
                                    '" + dt.idcreador + "',\n\
                                    '" + dt.activo + "'\n\
                                );";

                                    App.core.connection.query(q, function (err, open, fields) {
                                        pc++;
                                        if (!err) {
                                            if (pc === counter) {
                                                return runCallback(thenCallback);
                                            }
                                        } else {
                                            console.log(err);
                                            return runCallback(failCallback, err);
                                        }
                                    });
                                }
                            } else {
                                return runCallback(failCallback, err);
                            }
                        });
                        break;
                    case 5:
                        var q0 = "DELETE FROM fbr_especie WHERE idespecie >0";
                        App.core.connection.query(q0, function (err, open, fields) {
                            if (!err) {
                                for (var j = 0; j < data.fbr_especie.length; j++) {
                                    var dt = data.fbr_especie[j];
                                    var q = "INSERT INTO `fbr_especie` (\n\
                                    `idespecie`,\n\
                                    `descripcion`,\n\
                                    `codespecie`,\n\
                                    `moneda`,\n\
                                    `fecha_creacion`,\n\
                                    `idcreador`,\n\
                                    `activo`\n\
                                ) VALUES (\n\
                                    '" + dt.idespecie + "',\n\
                                    '" + dt.descripcion + "',\n\
                                    '" + dt.codespecie + "',\n\
                                    '" + dt.moneda + "',\n\
                                    '" + dt.fecha_creacion + "',\n\
                                    '" + dt.idcreador + "',\n\
                                    '" + dt.activo + "'\n\
                                );";

                                    App.core.connection.query(q, function (err, open, fields) {
                                        pc++;
                                        if (!err) {
                                            if (pc === counter) {
                                                return runCallback(thenCallback);
                                            }
                                        } else {
                                            return runCallback(failCallback, err);
                                        }
                                    });
                                }
                            } else {
                                return runCallback(failCallback, err);
                            }
                        });
                        break;
                    case 6:
                        var q0 = "DELETE FROM fbr_especie_acopio WHERE 1";
                        App.core.connection.query(q0, function (err, open, fields) {
                            if (!err) {
                                for (var j = 0; j < data.fbr_especie_acopio.length; j++) {
                                    var dt = data.fbr_especie_acopio[j];
                                    var q = "INSERT INTO `fbr_especie_acopio` (\n\
                                    `idespecie`,\n\
                                    `idacopio`\n\
                                ) VALUES (\n\
                                    '" + dt.idespecie + "',\n\
                                    '" + dt.idacopio + "'\n\
                                );";

                                    App.core.connection.query(q, function (err, open, fields) {
                                        pc++;
                                        if (!err) {
                                            if (pc === counter) {
                                                return runCallback(thenCallback);
                                            }
                                        } else {
                                            return runCallback(failCallback, err);
                                        }
                                    });
                                }
                            } else {
                                return runCallback(failCallback, err);
                            }
                        });
                        break;
                    case 7:
                        var q0 = "DELETE FROM fbr_parametro WHERE idparametro > 0";
                        App.core.connection.query(q0, function (err, open, fields) {
                            if (!err) {
                                for (var j = 0; j < data.fbr_parametro.length; j++) {
                                    var dt = data.fbr_parametro[j];
                                    var q = "INSERT INTO `fbr_parametro` (\n\
                                    `idparametro`,\n\
                                    `parametro`,\n\
                                    `grupo_parametro`,\n\
                                    `fecha_creacion`,\n\
                                    `idcreador`,\n\
                                    `activo`\n\
                                ) VALUES (\n\
                                    '" + dt.idparametro + "',\n\
                                    '" + dt.parametro + "',\n\
                                    '" + dt.grupo_parametro + "',\n\
                                    '" + dt.fecha_creacion + "',\n\
                                    '" + dt.idcreador + "',\n\
                                    '" + dt.activo + "'\n\
                                );";

                                    App.core.connection.query(q, function (err, open, fields) {
                                        pc++;
                                        if (!err) {
                                            if (pc === counter) {
                                                return runCallback(thenCallback);
                                            }
                                        } else {
                                            return runCallback(failCallback, err);
                                        }
                                    });
                                }
                            } else {
                                return runCallback(failCallback, err);
                            }
                        });
                        break;
                    case 8:
                        var q0 = "DELETE FROM fbr_param_especie WHERE 1";
                        App.core.connection.query(q0, function (err, open, fields) {
                            if (!err) {
                                for (var j = 0; j < data.fbr_param_especie.length; j++) {
                                    var dt = data.fbr_param_especie[j];
                                    var q = "INSERT INTO `fbr_param_especie` (\n\
                                    `idparametro`,\n\
                                    `idespecie`,\n\
                                    `posicion`\n\
                                ) VALUES (\n\
                                    '" + dt.idparametro + "',\n\
                                    '" + dt.idespecie + "',\n\
                                    '" + dt.posicion + "'\n\
                                );";

                                    App.core.connection.query(q, function (err, open, fields) {
                                        pc++;
                                        if (!err) {
                                            if (pc === counter) {
                                                return runCallback(thenCallback);
                                            }
                                        } else {
                                            return runCallback(failCallback, err);
                                        }
                                    });
                                }
                            } else {
                                return runCallback(failCallback, err);
                            }
                        });
                        break;
                    case 9:
                        var q0 = "DELETE FROM fbr_precio_semanal WHERE idprecio_semanal > 0";
                        App.core.connection.query(q0, function (err, open, fields) {
                            if (!err) {
                                for (var j = 0; j < data.fbr_precio_semanal.length; j++) {
                                    var dt = data.fbr_precio_semanal[j];
                                    var q = "INSERT INTO `fbr_precio_semanal` (\n\
                                    `idprecio_semanal`,\n\
                                    `semana_numero`,\n\
                                    `fecha_desde`,\n\
                                    `fecha_hasta`,\n\
                                    `idproducto`,\n\
                                    `idproductor`,\n\
                                    `idacopio`,\n\
                                    `precio_iqf`,\n\
                                    `precio_pulpa`,\n\
                                    `precio_iqf_usd`,\n\
                                    `precio_pulpa_usd`,\n\
                                    `fecha_creacion`,\n\
                                    `idcreador`,\n\
                                    `activo`\n\
                                ) VALUES (\n\
                                    '" + dt.idprecio_semanal + "',\n\
                                    '" + dt.semana_numero + "',\n\
                                    '" + dt.fecha_desde + "',\n\
                                    '" + dt.fecha_hasta + "',\n\
                                    '" + dt.idproducto + "',\n\
                                    '" + dt.idproductor + "',\n\
                                    '" + dt.idacopio + "',\n\
                                    '" + dt.precio_iqf + "',\n\
                                    '" + dt.precio_pulpa + "',\n\
                                    '" + dt.precio_iqf_usd + "',\n\
                                    '" + dt.precio_pulpa_usd + "',\n\
                                    '" + dt.fecha_creacion + "',\n\
                                    '" + dt.idcreador + "',\n\
                                    '" + dt.activo + "'\n\
                                );";

                                    App.core.connection.query(q, function (err, open, fields) {
                                        pc++;
                                        if (!err) {
                                            if (pc === counter) {
                                                return runCallback(thenCallback);
                                            }
                                        } else {
                                            return runCallback(failCallback, err);
                                        }
                                    });
                                }
                            } else {
                                return runCallback(failCallback, err);
                            }
                        });
                        break;
                    case 10:
                        var q0 = "DELETE FROM fbr_producto WHERE idproducto >0";
                        App.core.connection.query(q0, function (err, open, fields) {
                            if (!err) {
                                for (var j = 0; j < data.fbr_producto.length; j++) {
                                    var dt = data.fbr_producto[j];
                                    var q = "INSERT INTO `fbr_producto` (\n\
                                    `idproducto`,\n\
                                    `idvariedad`,\n\
                                    `codproducto`,\n\
                                    `descripcion`,\n\
                                    `tipo`,\n\
                                    `categoria`,\n\
                                    `clave`,\n\
                                    `cajas_pallet`,\n\
                                    `idbase_pallet`,\n\
                                    `porcentaje_merma`,\n\
                                    `fecha_creacion`,\n\
                                    `idcreador`,\n\
                                    `activo`\n\
                                ) VALUES (\n\
                                    '" + dt.idproducto + "',\n\
                                    '" + dt.idvariedad + "',\n\
                                    '" + dt.codproducto + "',\n\
                                    '" + dt.descripcion + "',\n\
                                    '" + dt.tipo + "',\n\
                                    '" + dt.categoria + "',\n\
                                    '" + dt.clave + "',\n\
                                    '" + dt.cajas_pallet + "',\n\
                                    '" + dt.idbase_pallet + "',\n\
                                    '" + dt.porcentaje_merma + "',\n\
                                    '" + dt.fecha_creacion + "',\n\
                                    '" + dt.idcreador + "',\n\
                                    '" + dt.activo + "'\n\
                                );";

                                    App.core.connection.query(q, function (err, open, fields) {
                                        pc++;
                                        if (!err) {
                                            if (pc === counter) {
                                                return runCallback(thenCallback);
                                            }
                                        } else {
                                            return runCallback(failCallback, err);
                                        }
                                    });
                                }
                            } else {
                                return runCallback(failCallback, err);
                            }
                        });
                        break;
                    case 11:
                        var q0 = "DELETE FROM fbr_variedad WHERE idvariedad > 0";
                        App.core.connection.query(q0, function (err, open, fields) {
                            if (!err) {
                                for (var j = 0; j < data.fbr_variedad.length; j++) {
                                    var dt = data.fbr_variedad[j];
                                    var q = "INSERT INTO `fbr_variedad` (\n\
                                    `idvariedad`,\n\
                                    `idespecie`,\n\
                                    `descripcion`,\n\
                                    `codvariedad`,\n\
                                    `fecha_creacion`,\n\
                                    `idcreador`,\n\
                                    `activo`\n\
                                ) VALUES (\n\
                                    '" + dt.idvariedad + "',\n\
                                    '" + dt.idespecie + "',\n\
                                    '" + dt.descripcion + "',\n\
                                    '" + dt.codvariedad + "',\n\
                                    '" + dt.fecha_creacion + "',\n\
                                    '" + dt.idcreador + "',\n\
                                    '" + dt.activo + "'\n\
                                );";
                                    App.core.connection.query(q, function (err, open, fields) {
                                        pc++;
                                        if (!err) {
                                            if (pc === counter) {
                                                return runCallback(thenCallback);
                                            }
                                        } else {
                                            return runCallback(failCallback, err);
                                        }
                                    });
                                }
                            } else {
                                return runCallback(failCallback, err);
                            }
                        });
                        break;
                }
            }
        });

    };

    function runCallback(callbackToRun, args) {
        console.log(args);
        if (callbackToRun && typeof callbackToRun === 'function') {
            callbackToRun(args);
        }
    }
}

function Get() {

    var self = this;
    var thenCallback = null;
    var failCallback = null;
    var data = null;

    self.then = function (callback) {
        thenCallback = callback;
        return self;
    };

    self.error = function (callback) {
        failCallback = callback;
        return self;
    };

    self.complete = function (data, requestdata) {
        self.data = data;

        self.loadpreformeddata(function (data) {
            var ndata = {};
            self.data.performeddata = data;
            console.log(self.data);
            return runCallback(thenCallback, btoa(JSON.stringify(self.data)));
        });
    };

    self.loadpreformeddata = function (continueCallback) {
        var preformedData = {
            fbr_recepcion: [],
            fbr_recepcion_calidad: [],
            fbr_recepcion_linea: [],
            fbr_recepcion_pallet: [],
            fbr_recepcion_pallet_pesaje: [],
            fbr_recepcion_parametro_valor: [],
            fbr_recepcion_pesaje: [],
            fbr_despacho: [],
            fbr_despacho_detalle: [],
            fbr_mov_envase: [],
            fbr_mov_envase_detalle: []
        }, i = 0;
        App.core.loadMysql(function () {
            var q1 = "SELECT * FROM `fbr_recepcion`;";
            var q2 = "SELECT * FROM `fbr_recepcion_calidad`;";
            var q3 = "SELECT * FROM `fbr_recepcion_linea`;";
            var q4 = "SELECT * FROM `fbr_recepcion_pallet`;";
            var q5 = "SELECT * FROM `fbr_recepcion_pallet_pesaje`;";
            var q6 = "SELECT * FROM `fbr_recepcion_parametro_valor`;";
            var q7 = "SELECT * FROM `fbr_recepcion_pesaje`;";
            var q8 = "SELECT * FROM `fbr_despacho`;";
            var q9 = "SELECT * FROM `fbr_despacho_detalle`;";
            var qa = "SELECT * FROM `fbr_mov_envase`;";
            var qb = "SELECT * FROM `fbr_mov_envase_detalle`;";
            App.core.connection.query(q1, function (err, row, fields) {
                if (!err) {
                    preformedData.fbr_recepcion = row;
                    App.core.connection.query(q2, function (err, row, fields) {
                        if (!err) {
                            preformedData.fbr_recepcion_calidad = row;
                            App.core.connection.query(q3, function (err, row, fields) {
                                if (!err) {
                                    preformedData.fbr_recepcion_linea = row;
                                    App.core.connection.query(q4, function (err, row, fields) {
                                        if (!err) {
                                            preformedData.fbr_recepcion_pallet = row;
                                            App.core.connection.query(q5, function (err, row, fields) {
                                                if (!err) {
                                                    preformedData.fbr_recepcion_pallet_pesaje = row;
                                                    App.core.connection.query(q6, function (err, row, fields) {
                                                        if (!err) {
                                                            preformedData.fbr_recepcion_parametro_valor = row;
                                                            App.core.connection.query(q7, function (err, row, fields) {
                                                                if (!err) {
                                                                    preformedData.fbr_recepcion_pesaje = row;
                                                                    App.core.connection.query(q8, function (err, row, fields) {
                                                                        if (!err) {
                                                                            preformedData.fbr_despacho = row;
                                                                            App.core.connection.query(q9, function (err, row, fields) {
                                                                                if (!err) {
                                                                                    preformedData.fbr_despacho_detalle = row;
                                                                                    App.core.connection.query(qa, function (err, row, fields) {
                                                                                        if (!err) {
                                                                                            preformedData.fbr_mov_envase = row;
                                                                                            App.core.connection.query(qb, function (err, row, fields) {
                                                                                                if (!err) {
                                                                                                    preformedData.fbr_mov_envase_detalle = row;
                                                                                                    continueCallback(preformedData);
                                                                                                } else {
                                                                                                    runCallback(failCallback, err);
                                                                                                }
                                                                                            });
                                                                                        } else {
                                                                                            runCallback(failCallback, err);
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    runCallback(failCallback, err);
                                                                                }
                                                                            });
                                                                        } else {
                                                                            runCallback(failCallback, err);
                                                                        }
                                                                    });
                                                                } else {
                                                                    runCallback(failCallback, err);
                                                                }
                                                            });
                                                        } else {
                                                            runCallback(failCallback, err);
                                                        }
                                                    });
                                                } else {
                                                    runCallback(failCallback, err);
                                                }
                                            });
                                        } else {
                                            runCallback(failCallback, err);
                                        }
                                    });
                                } else {
                                    runCallback(failCallback, err);
                                }
                            });
                        } else {
                            runCallback(failCallback, err);
                        }
                    });
                } else {
                    runCallback(failCallback, err);
                }
            });
        });
    };

    function runCallback(callbackToRun, args) {
        if (callbackToRun && typeof callbackToRun === 'function') {
            console.log(args);
            callbackToRun(args);
        }
    }

}
