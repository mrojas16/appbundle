/* global App */
App.oninit = function () {
    App.core.storage.get('dbconnection', function (error, data) {
        if (error) {
            $.gritter.add({
                title: "Error",
                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos de MYSQL</i>",
                class_name: 'growl-danger',
                sticky: false,
                time: 4000
            });
        } else {
            $("[name=ip_serv]").val(data.ip_serv);
            $("[name=user]").val(data.user);
            $("[name=pass]").val(data.pass);
            $("[name=schema]").val(data.schema);
            $("[name=port]").val(data.port);
        }
    });
    App.core.storage.get('apiconnection', function (error, data) {
        if (error) {
            $.gritter.add({
                title: "Error",
                text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error obteniendo los datos de la API</i>",
                class_name: 'growl-danger',
                sticky: false,
                time: 4000
            });
        } else {
            $("[name=api_serv]").val(data.api_serv);
            $("[name=api_user]").val(data.api_user);
            $("[name=api_pass]").val(data.api_pass);
        }
    });
};

$("#primal-form").validate({
    rules: {
        ip_serv: {
            required: true
        },
        user: {
            required: true
        },
        pass: {
            required: true
        },
        schema: {
            required: true
        },
        port: {
            required: true
        }
    },
    messages: {
        ip_serv: {
            required: "Ingrese una direccion de servidor"
        },
        user: {
            required: "Ingrese el usuario de la base de datos"
        },
        pass: {
            required: "Ingrese la contraseña de la base de datos"
        },
        schema: {
            required: "Ingrese el esquema de la conexión"
        },
        port: {
            required: "Ingrese el puerto de la conexión"
        }
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (f) {
        var form = $(f);
        App.core.storage.set('dbconnection', {
            ip_serv: $("[name=ip_serv]", form).val(),
            user: $("[name=user]", form).val(),
            pass: $("[name=pass]", form).val(),
            schema: $("[name=schema]", form).val(),
            port: $("[name=port]", form).val()
        }, function (error) {
            if (error) {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 4000
                });
            } else {
                $.gritter.add({
                    title: "OK!",
                    text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                    class_name: 'growl-success',
                    sticky: false,
                    time: 2000
                });
            }
        });
    }
});

$("#primal-form2").validate({
    rules: {
        api_serv: {
            required: true
        },
        api_user: {
            required: true
        },
        api_pass: {
            required: true
        }
    },
    messages: {
        api_serv: {
            required: "Ingrese una direccion de servidor"
        },
        api_user: {
            required: "Ingrese el usuario"
        },
        api_pass: {
            required: "Ingrese la KEY"
        }
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        element.after(error);
    },
    submitHandler: function (f) {
        var form = $(f);
        App.core.storage.set('apiconnection', {
            api_serv: $("[name=api_serv]", form).val(),
            api_user: $("[name=api_user]", form).val(),
            api_pass: $("[name=api_pass]", form).val()
        }, function (error) {
            if (error) {
                $.gritter.add({
                    title: "Error",
                    text: "<i class='fa fa-clock-o'></i> <i>Ha Ocurrido un error guardando los datos</i>",
                    class_name: 'growl-danger',
                    sticky: false,
                    time: 4000
                });
            } else {
//                App.remote.getCurrentWindow().reload();
//                exec(process.argv.join(' '))
//                setTimeout(function () {
//                    var e = require('electron');
//                    var r = e.remote;
//                    var w = r.getCurrentWindow();
//                    w.close();
//                }, 50)
                $.gritter.add({
                    title: "OK!",
                    text: "<i class='fa fa-clock-o'></i> <i>Se han guardado correctamente los datos</i>",
                    class_name: 'growl-success',
                    sticky: false,
                    time: 2000
                });
            }
        });
    }
});
